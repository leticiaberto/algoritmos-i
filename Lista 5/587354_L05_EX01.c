/* ================================================================== *
    Universidade Federal de Sao Carlos - UFSCar, Sorocaba

    Disciplina: Algoritmos e Programação 1
    Prof. Tiago A. Almeida

    Lista 05 - Exercício 01 - Sequencia de Fibonacci

    Instrucoes
    ----------

    Este arquivo contem o codigo que auxiliara no desenvolvimento do
    exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
    Dados do aluno:

    RA: 587354
    Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>

/* ================================================================== */
/* ======================== ROTINA PRINCIPAL ======================== */
/* ================================================================== */

int main(){
	
	int n,aux1,aux2,soma,cont;
	
	do
		scanf ("%d",&n);
	while (n<1);

	soma = 0;
	aux2=0;
	aux1=1;
	cont = 0;

	while (cont<n)
	{
		aux2=aux1;
		aux1=soma;
		soma=aux1 + aux2;
		cont++;
	}//while
	
	printf ("%d\n",soma);
    return (0);
}
