/* ================================================================== *
    Universidade Federal de Sao Carlos - UFSCar, Sorocaba

    Disciplina: Algoritmos e Programação 1
    Prof. Tiago A. Almeida

    Lista 05 - Exercício 08 - Calculadora Finanças

    Instrucoes
    ----------

    Este arquivo contem o codigo que auxiliara no desenvolvimento do
    exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
    Dados do aluno:

    RA: 587354
    Nome: Leticia Mara Berto

* ================================================================== */

#include <stdio.h>
#include <math.h>

//Possiveis opcoes do menu
#define OPC_TEMPO	1
#define OPC_VALOR	2
#define OPC_INVEST 	3
#define OPC_SAIR 	4

//Frase exibida para o caso de erro
#define OPC_INEXISTENTE	"opcao inexistente\n"   

/* ================================================================== */
/* ======================== ROTINA PRINCIPAL ======================== */
/* ================================================================== */

int main() 
{
	int opc = 0; ////Armazena a opção selecionada pelo usuário.
	double inves_inici,mont_desej,perc_juros,result,tempo,frac;
	
	/* DECLARE AQUI AS OUTRAS VARIÁVEIS QUE SERÃO UTILIZADAS*/


	//Verifica se o usuário não selecionou a opção "4 - Sair" para prosseguir com o programa..
	while(opc != OPC_SAIR)
	{
		scanf("%d", &opc); //Lê a opção selecionada.
		
		//zera as variaveis para fazer novos calculos
		inves_inici =0;
		mont_desej =0;
		perc_juros =0;
		result =0;
		tempo = 0;
		frac = 0;

		//Verifica se o usuário digitou uma opção válida (1 a 4).
		if (opc < OPC_TEMPO || opc > OPC_SAIR)
			printf(OPC_INEXISTENTE);
		else 
		{
			switch(opc)
			 {
				case OPC_TEMPO: //Tempo Necessário
					scanf ("%lf",&inves_inici);
					scanf ("%lf",&mont_desej);
					scanf ("%lf",&perc_juros);
					perc_juros = perc_juros/100;
					tempo = (log10(mont_desej/inves_inici))/(log10(1+perc_juros));
					frac = tempo - (int)tempo;
					if (frac !=0)
						printf ("%d\n",(int) tempo + 1);//arredondamento
					else
						printf("%lf\n",tempo);
					break;

				case OPC_VALOR:
					scanf ("%lf",&inves_inici);
					scanf ("%lf",&perc_juros);
					scanf ("%lf",&tempo);
					perc_juros = perc_juros/100;
					result = inves_inici*(pow((1+perc_juros),tempo));
					printf("%.2lf\n",result);
					break;

				case OPC_INVEST:
					scanf ("%lf",&mont_desej);
					scanf ("%lf",&perc_juros);
					scanf ("%lf",&tempo);
					perc_juros = perc_juros/100;
					result = mont_desej/(pow((1+perc_juros),tempo));
					printf("%.2lf\n",result);
					break;				
				/* COMPLETAR AQUI */


				default:
					return (0);
			}
		}
	}
	
	return (0);	
}
