/* ================================================================== *
    Universidade Federal de Sao Carlos - UFSCar, Sorocaba

    Disciplina: Algoritmos e Programação 1
    Prof. Tiago A. Almeida

    Lista 05 - Exercício 03 - Calculadora Simples

    Instrucoes
    ----------

    Este arquivo contem o codigo que auxiliara no desenvolvimento do
    exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
    Dados do aluno:

    RA: 587354
    Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>

//Possiveis opcoes do menu
#define OPC_SOMA	1
#define OPC_SUB		2
#define OPC_MULT 	3
#define OPC_DIV 	4
#define OPC_POT 	5
#define OPC_FAT 	6
#define OPC_SAIR 	7

//Definicao de resultados booleanos
#define VERDADEIRO	1
#define FALSO		2    

//Frases exibidas para os casos de erro
#define OPC_INEXISTENTE	"Erro: opcao inexistente.\n"   
#define DIV_ZERO 		"Erro: divisao por zero.\n"
#define FAT_NEGATIVO 	"Erro: fatorial de numero negativo.\n"

/* ================================================================== */
/* ======================== ROTINA PRINCIPAL ======================== */
/* ================================================================== */

int main(){
	int opc = 0, //Armazena a opção selecionada pelo usuário.
		i, 		 //Contador para os laços.
		expoente, //Expoente para as operações de potenciação.
		fatorial, //Base para as operações de fatorial.
		flag_ok = VERDADEIRO; //flag utilizada para indicar se houve entrada incorreta de divisor (= 0) ou base do fatorial (< 0)  

	float v1, v2,aux,aux1,//Valores para os cálculos (soma, subtração, ...).
	resultado = 1.0; //Armazena o resultado da operações de potência e fatorial.
	
	//Verifica se o usuário não selecionou a opção "7 - Sair" para prosseguir com o programa..
	while(opc != OPC_SAIR){
		scanf("%d", &opc); //Lê a opção selecionada.

		//Verifica se o usuário digitou uma opção válida (1 a 7).
		if (opc < OPC_SOMA || opc > OPC_SAIR)
			printf(OPC_INEXISTENTE);
		else {
			// Realiza a operacao desejada
			switch(opc) {
				//Soma
				case OPC_SOMA:
					scanf ("%f",&v1);
					scanf ("%f",&v2);
					resultado = v1 + v2;
					break;
				//Subtração
				case OPC_SUB:
					scanf ("%f",&v1);
					scanf ("%f",&v2);
					resultado = v1 - v2;
					break;	
				//multiplicação
				case OPC_MULT:
					scanf ("%f",&v1);
					scanf ("%f",&v2);
					resultado = v1 * v2;
					break;	
				//Divisão
				case OPC_DIV:
					scanf ("%f",&v1);
					scanf ("%f",&v2);
					if (v2==0)
					{
						printf(DIV_ZERO);
						flag_ok = FALSO;
					}
					else
						resultado = v1 / v2;
					break;
				//Potenciação
				case OPC_POT:
					scanf ("%f",&v1);
					scanf ("%f",&v2);
					
					if (v2==0)
					{
						resultado = resultado;
					}
					else
					if (v2>0)
					{
						expoente = 1;
						resultado=v1;
						//i=1;
						while (expoente<v2)
						{
							resultado = resultado * v1;
							expoente++;
						}//while
					}//else
					else 
					if (v2<0)
					{
						expoente =v2 + 1;
						//resultado=v1;
						//i =0;
						i=v1;
						while (expoente<0)
						{
							i = i * v1;
							//printf ("%d\n",i);
							//resultado = 1 / i;
							expoente++;
											
						}//while
						aux1=i;
						aux = 1 / aux1;
						resultado = aux;
					}
					break;
				//Fatorial
				case OPC_FAT:
					scanf ("%f",&v1);
					fatorial = v1;
					if (v1<0)
					{
						printf(FAT_NEGATIVO);
						flag_ok = FALSO;
					}	 
					/*if ((v1 ==1) || (v1 ==0))
					{
						resultado = resultado;
					}*/
					else
					{	i = v1-1;
						fatorial = v1;
						while (i>=1)
						{
							fatorial = fatorial*i;
							resultado = fatorial;
							i--;
						}//while
					}//else
					break;	
				default:
					return (0);
			}

			//Exibe o resultado somente se nao houve entrada incorreta
			if (flag_ok == VERDADEIRO){
				printf("%.2f\n", resultado);
			}

			//"Limpa" a variável resultado para a proxima operação
			resultado = 1.0;

			//"Limpa" a variável flag para a proxima operação
			flag_ok = VERDADEIRO;
		}
	}

	return (0);
}
