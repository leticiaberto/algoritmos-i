/* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 1
	Prof. Tiago A. Almeida

	Lista 05 - Exercício 06 - Números Primos

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354
	Nome: Leticia Mara Berto

* ================================================================== */

#include <stdio.h> 

/* ================================================================== */
/* ======================== ROTINA PRINCIPAL ======================== */
/* ================================================================== */

int main()
{
	int n,cont,dive,flag,j,aux;
	cont = 2;
	dive = 2;
	j = 2;
	scanf ("%d",&n);
	flag=1;
	
	while (n<2)//para ser primo tem que ser maior q 2 (condição do exercicio)
	{
		scanf ("%d",&n);
	}//while
	
	while (cont<n)
	{
		 if (n==2)//não tem primo menor que 2
	         {
			break;
		 }

        	j = 2;
        
		while (j < dive) 
		{
		    aux = dive % j;
		    //printf ("%d",aux);
		    if ( aux == 0)
		    {
			flag = 0;
			//printf ("entrou");
		    }  

		    j = j + 1;

		}//while

		if (flag==1) 
		    {
			//flag = 1;
			printf ("%d ",j);
		    }   
		
	        cont = cont + 1;
	        dive = dive+1;
	        flag = 1;

    	}//while

	return (0);	
}	
