/* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 1
	Prof. Tiago A. Almeida

	Lista 05 - Exercício 05 - Subsequência

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354
	Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h> 

/* ================================================================== */
/* ======================== ROTINA PRINCIPAL ======================== */
/* ================================================================== */

int main()
{
	int n,soma,aux,seq,qntd;

	scanf ("%d",&n);
	soma = 0;
	aux = -99999;//para poder usar numeros negativos
	qntd = 1;
	
	while (n!=0)
	{
		seq = aux;		
		aux = n;
		if (qntd<=5)
		{
			if (n>seq)
			{
				soma = soma + n;
				qntd = qntd + 1;
				//printf ("entrou");
			}//
			else
			{
				soma = 0;//zera a soma ao quebrar a seq
				qntd = 2;//quebra a sequencia para o contador de qntd e caso volte a sequencia ja conta o primeiro numero da seq
			}//else
			if (soma==0)
			{
				soma = n;//para a soma sempre valer o ultimo valor informado
			}
		}//if
		//seq = aux;
		//printf("\nTermo lido: %d, Soma atual = %d, Quantidade = %d, Termo anterior = %d\n", n, soma, qntd, seq);
		scanf ("%d",&n);		
	}//while
	if (qntd==6)//porque o contador ja começa com 1
	{
		printf ("%d",soma);
	}
	else 
	{
		soma = 0;
		printf ("%d",soma);
	}
	return (0);	
}	
