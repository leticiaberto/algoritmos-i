/* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 1
	Prof. Tiago A. Almeida

	Lista 05 - Exercício 04 - Anos Bissextos

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354
	Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>

//Definindo a frase de erro
#define FRASE_ERRO "Erro: O primeiro ano deve ser menor que o segundo.\n" 

/* ================================================================== */
/* ======================== ROTINA PRINCIPAL ======================== */
/* ================================================================== */

int main()
{
	int ano1, ano2, qtde_bissextos;
	
	scanf("%d", &ano1);
	scanf("%d", &ano2);

	
		
		while ((ano1 !=0) || (ano2 !=0))
		{	
			if (ano1>ano2)
			{
				printf(FRASE_ERRO);
			}
			else
			{
				qtde_bissextos = 0;
				while (ano1<=ano2)
				{
					
					
					 
					 if (ano1 % 4 == 0) 
					{
						if ((ano1 % 100==0) && (ano1 % 400==0))
						{
						printf ("%d ",ano1);
						qtde_bissextos = qtde_bissextos + 1;
						}//if
						else
						{	
							if ((ano1 % 4 == 0) && (ano1 % 100!=0))
							{
								printf ("%d ",ano1);
								qtde_bissextos = qtde_bissextos + 1;
							}
						}//else
					}//if
					ano1++;
				}//while
				printf ("\n%d\n",qtde_bissextos);
			}//else
			
			scanf("%d", &ano1);
			scanf("%d", &ano2);
		}//while	

	
	return (0);
}	
