/* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 1
	Prof. Tiago A. Almeida

	Lista 05 - Exercício 02 - Controle de pedidos de lanchonete

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354
	Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>

//Definindo as frases de saída de cada produto
#define FRASE_CACHORRAO 		"%d x Cachorro quente: R$ %.2f\n"
#define FRASE_HAMBURGER 		"%d x Hamburger: R$ %.2f\n"
#define FRASE_CHEESEBURGER 		"%d x Cheeseburger: R$ %.2f\n"
#define FRASE_EGGCHEESEBURGER	"%d x Eggcheeseburger: R$ %.2f\n"
#define FRASE_REFRIGERANTE 		"%d x Refrigerante: R$ %.2f\n"
#define FRASE_PERSONALIZADO 	"%d x Personalizado: R$ %.2f\n"
#define FRASE_TOTAL 			"Total do pedido: R$ %.2f\n"

//Definindo a mensagem de erro de codigo invalido 
#define FRASE_CODIGO_INVALIDO "Código inválido!\n"

//Definindo opcoes do menu principal
#define SAIR				0
#define CACHORRAO 			1 
#define HAMBURGUER			2
#define CHEESEBURGUER 		3
#define EGGCHEESEBURGUER	4
#define REFRIGERANTE 		5
#define PERSONALIZADO		6	

//Definindo opcoes do menu personalizado
#define VOLTAR			0
#define HAMBURGUER_PERS	1 
#define BATATA_PALHA	2
#define ALFACE			3
#define CATUPIRY		4
#define OVO				5
#define CALABRESA		6

//Definindo os preços unitários de cada produto
#define PRECO_CACHORRAO			1.5
#define PRECO_HAMBURGER 		2
#define PRECO_CHEESEBURGER 		2.5
#define PRECO_EGGCHEESEBURGER	3
#define PRECO_REFRIGERANTE 		1.5
#define PRECO_PERSONALIZADO 	0.5

/* ================================================================== */
/* ======================== ROTINA PRINCIPAL ======================== */
/* ================================================================== */

int main(){
	/* NAO PODE CRIAR E NEM REMOVER VARIAVEIS */
	int quantidade, quantidade_personalizado=0, opcao_lanches, opcao_personalizado;
	float total_cachorrao=0.0;
	float total_hamburger=0.0; 
	float total_cheeseburger=0.0; 
	float total_eggcheeseburger=0.0; 
	float total_refrigerante=0.0; 
	float total_personalizado=0.0; 
	float total_geral=0.0;
	
	// le a opcao de lanche
	scanf("%d", &opcao_lanches);

	while (opcao_lanches != SAIR){
		switch (opcao_lanches){
			case CACHORRAO:
				scanf("%d",&quantidade);
				total_cachorrao = quantidade * PRECO_CACHORRAO; 
				printf(FRASE_CACHORRAO, quantidade, total_cachorrao);
				total_geral = total_geral + total_cachorrao;
				break;
			
			case HAMBURGUER:
				scanf ("%d",&quantidade);
				total_hamburger = quantidade * PRECO_HAMBURGER;
				printf(FRASE_HAMBURGER, quantidade, total_hamburger);
				total_geral = total_geral + total_hamburger;
				break;
			
			case CHEESEBURGUER:
				scanf ("%d",&quantidade);
				total_cheeseburger = quantidade * PRECO_CHEESEBURGER;
				printf(FRASE_CHEESEBURGER, quantidade, total_cheeseburger);
				total_geral = total_geral + total_cheeseburger;
				break;

			case EGGCHEESEBURGUER:
				scanf ("%d",&quantidade);
				total_eggcheeseburger = quantidade * PRECO_EGGCHEESEBURGER;
				printf(FRASE_EGGCHEESEBURGER, quantidade, total_eggcheeseburger);
				total_geral = total_geral + total_eggcheeseburger;
				break;

			case REFRIGERANTE:
				scanf ("%d",&quantidade);
				total_refrigerante = quantidade * PRECO_REFRIGERANTE;
				printf(FRASE_REFRIGERANTE, quantidade, total_refrigerante);
				total_geral = total_geral + total_refrigerante;
				break;
			case PERSONALIZADO:
				scanf ("%d",&opcao_personalizado);
				quantidade = 0;
				while (opcao_personalizado != VOLTAR)
				{
					switch (opcao_personalizado)
					{ 
						case HAMBURGUER_PERS:
							scanf ("%d",&quantidade_personalizado);
							quantidade = quantidade_personalizado + quantidade;
							total_personalizado = total_personalizado + quantidade_personalizado * 0.5;
							break;
						case BATATA_PALHA:
							scanf ("%d",&quantidade_personalizado);
						        quantidade = quantidade_personalizado + quantidade;
							total_personalizado = total_personalizado + quantidade_personalizado * 0.5;
							break;
						case ALFACE:
							scanf ("%d",&quantidade_personalizado);
							quantidade = quantidade_personalizado + quantidade;
							total_personalizado = total_personalizado + quantidade_personalizado * 0.5;
							break;
						case CATUPIRY:
							scanf ("%d",&quantidade_personalizado);
							quantidade = quantidade_personalizado + quantidade;
							total_personalizado = total_personalizado + quantidade_personalizado * 0.5;
							break;
						case OVO:
							scanf ("%d",&quantidade_personalizado);
							quantidade = quantidade_personalizado + quantidade;
							total_personalizado = total_personalizado + quantidade_personalizado * 0.5;
							break;
						case CALABRESA:
							scanf ("%d",&quantidade_personalizado);
							quantidade = quantidade_personalizado + quantidade;
							total_personalizado = total_personalizado + quantidade_personalizado * 0.5;
							break;
						default: printf(FRASE_CODIGO_INVALIDO);
					
					}//switch
				
				scanf ("%d",&opcao_personalizado);
				
				}//while personalizado
				printf(FRASE_PERSONALIZADO, quantidade,total_personalizado);
				break;

			default: printf(FRASE_CODIGO_INVALIDO);
			
		}
		scanf("%d", &opcao_lanches);		
	}
	// impressao final do valor total do pedido
	total_geral = total_geral + total_personalizado;
	//total_geral = total_geral + quantidade_personalizado;
	printf(FRASE_TOTAL, total_geral);

	return (0);
}
