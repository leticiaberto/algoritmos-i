/* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 1
	Prof. Tiago A. Almeida

	Lista 05 - Exercício 07 - Método de Heron

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354
	Nome: Leticia Mara Berto

* ================================================================== */

#include <stdio.h> 

/* ================================================================== */
/* ======================== ROTINA PRINCIPAL ======================== */
/* ================================================================== */

int main()
{
	int n1,n2,cont,intervalo;
	float k,kn;
	scanf ("%d",&n1);
	scanf ("%d",&n2);

	k=0;
	kn=1;//valor inicial dado no enunciado
	cont =1;
	
		
	if (n1<n2)
	{
		intervalo=n1;

		while (intervalo<=n2)
		{	
			while (cont<=10)//fazer 10 vezes para um resultado mais preciso
			{
				//kn=kn+1;
				k=(kn+(intervalo/kn))/2;//o intervalo assume os valores entre n1 e n2
				kn=k;
				cont=cont+1;
			}//while raiz

			printf ("%.2f\n",k);

			//atribui os valores iniciais para fazer o calculo do proximo valor do intervalo
			k=0;
			kn=1;//valor inicial dado no enunciado
			cont=1;
			intervalo=intervalo+1;

		}//while
		return (0);//finaliza o prog pq acaba a condição e nao entra em if(n1=n2),pois sai daqui com n1=n2
	}//if n1<n2	

	if (n1>n2)
	{
		intervalo=n2;

		while (intervalo<=n1)
		{	
			while (cont<=10)//fazer 10 vezes para um resultado mais preciso
			{
				//kn=kn+1;
				k=(kn+(intervalo/kn))/2;//o intervalo assume os valores entre n1 e n2
				kn=k;
				cont=cont+1;
			}//while raiz

			printf ("%.2f\n",k);

			//atribui os valores iniciais para fazer o calculo do proximo valor do intervalo
			k=0;
			kn=1;//valor inicial dado no enunciado
			cont=1;
			intervalo=intervalo+1;

		}//while
		return (0);//finaliza o prog pq acaba a condição e nao entra em if(n1=n2),pois sai daqui com n1=n2
	}// n1>n2

	if (n1=n2)
	{
			
			while (cont<=10)//fazer 10 vezes para um resultado mais preciso
			{
				//kn=kn+1;
				k=(kn+(n1/kn))/2;//o intervalo assume os valores entre n1 e n2
				kn=k;
				cont=cont+1;
			}//while raiz

			printf ("%.2f\n",k);
			return (0);//finaliza o prog pq acaba a condição e nao entra em if(n1=n2),pois sai daqui com n1=n2		
	}//if n1=n2
}//main
