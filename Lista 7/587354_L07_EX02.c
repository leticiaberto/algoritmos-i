/* ================================================================== *
    Universidade Federal de Sao Carlos - UFSCar, Sorocaba

    Disciplina: Algoritmos e Programação 1
    Prof. Tiago A. Almeida

    Lista 07 - Exercício 02 - Calculadora de números complexos

    Instrucoes
    ----------

    Este arquivo contem o codigo que auxiliara no desenvolvimento do
    exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
    Dados do aluno:

    RA: 587354
    Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>


//Definindo a frase de opção inválida
#define FRASE_OPCAO_INVALIDA "Erro: Opção inválida!\n"

//Valores de controle
#define FALSE   0
#define TRUE    1

//Estrutura para armazenar os números complexos.
/* <<< DEFINA AQUI A ESTRUTURA (Complexo) PARA ARMAZENAR OS NÚMEROS COMPLEXOS >>> */
typedef struct _complexo
{
	float real;
	float img;
}Complexo;
/* ================================================================== */
/* ======================== ROTINA PRINCIPAL ======================== */
/* ================================================================== */

int main () {
    int opcao; //opção desejada do menu 

    Complexo num1,   //primeiro número complexo
             num2,   //segundo número complexo
             result; //resultado das operações
    /* <<< DECLARE MAIS VARIAVEIS SE FOR NECESSARIO >>> */             

    /* <<< COMPLETE AQUI >>> */

	//recebe o primeiro numero complexo
 	scanf ("%f",&num1.real);
    	scanf ("%f",&num1.img);

	//recebe o segundo numero complexo
	scanf ("%f",&num2.real);
	scanf ("%f",&num2.img);

	//recebe a operação desejada
	scanf ("%d",&opcao);

	while (opcao!=5)
	{	
		result.real=0;
		result.img=0;

		switch (opcao)
		{
			case 1:
				result.real=num1.real+num2.real;
				result.img=num1.img+num2.img;
				//printf ("%.2f+%.2fi\n",result.real,result.img);
				if (result.img>0)
				printf ("%.2f+%.2fi\n",result.real,result.img);
				else
				printf ("%.2f%.2fi\n",result.real,result.img);
				break;
			case 2:
				result.real=num1.real-num2.real;
				result.img=num1.img-num2.img;
				if (result.img>0)
				printf ("%.2f+%.2fi\n",result.real,result.img);
				else
				printf ("%.2f%.2fi\n",result.real,result.img);
				break;
			case 3:
				result.real=num1.real*num2.real + num1.img*num2.img ;
				result.img=num1.real*num2.img + num1.img*num2.real;
				if (result.img>0)
				printf ("%.2f+%.2fi\n",result.real,result.img);
				else
				printf ("%.2f%.2fi\n",result.real,result.img);
				break;
			case 4:
				//recebe o primeiro numero complexo
			 	scanf ("%f",&num1.real);
			    	scanf ("%f",&num1.img);

				//recebe o segundo numero complexo
				scanf ("%f",&num2.real);
				scanf ("%f",&num2.img);
				break;
			default:
				printf (FRASE_OPCAO_INVALIDA);
				break;

		}//switch
		scanf ("%d",&opcao);
	}//while
        return 0;
} 
