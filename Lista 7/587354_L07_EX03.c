/* ================================================================== *
    Universidade Federal de Sao Carlos - UFSCar, Sorocaba

    Disciplina: Algoritmos e Programação 1
    Prof. Tiago A. Almeida

    Lista 07 - Exercício 03 - Retas

    Instrucoes
    ----------

    Este arquivo contem o codigo que auxiliara no desenvolvimento do
    exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
    Dados do aluno:

    RA: 587354
    Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>
#include <math.h>

//Estrutura que armazena os dados de um ponto.
/* <<< DEFINA AQUI A ESTRUTURA (Ponto) PARA ARMAZENAR OS PONTOS DA RETA >>> */
typedef struct _ponto
{
	float x;
	float y;
}Ponto;
//Estrutura aninhada, armazena informações sobre um segmento de reta.
/* <<< DEFINA AQUI A ESTRUTURA (Reta) PARA ARMAZENAR CADA RETA >>> */
typedef struct _reta
{
	Ponto p1;
	Ponto p2;
}Reta;
#define NAO_EXISTE_INTERCESSAO 		0
#define EXISTE_INTERCESSAO 			1
#define COEFICIENTE_NAO_DEFINIDO 	0
#define COEFICIENTE_DEFINIDO 		1
#define MENSAGEM_COEFICIENTE_NAO_DEFINIDO "indefinido"

int main(){
	Reta r1, r2; //Armazenam as informações de ambos os segmentos de reta.
	Ponto pontoInterseccao; //Armazena o ponto de interseccao entre os dois segmentos, se existir.
	float dx1, dx2, dy1, dy2; //Armazenam os comprimentos dos catetos dos triângulos formados pelas retas.
	float coeficiente1, coeficiente2; //Armazenam os coeficientes angulares das retas;
	float comprimento1, comprimento2; //Armazenam os comprimentos das retas.
	//float y1,y2; //Armazenam as equações das retas
	//float py1,px1,px2,py2; //Armazenam um ponto qualquer da reta
	float x,y; //Armazena o x,y em comum das equações
	int flaginter,flagcoef1,flagcoef2;
	/* <<< DECLARE MAIS VARIAVEIS SE FOR NECESSARIO >>> */

	/* <<< COMPLETE AQUI >>> */
	//recebe os valores da primeira reta
	scanf ("%f %f",&r1.p1.x,&r1.p1.y);
	scanf ("%f %f",&r1.p2.x,&r1.p2.y);

	//recebe os valores da segunda reta
	scanf ("%f %f",&r2.p1.x,&r2.p1.y);
	scanf ("%f %f",&r2.p2.x,&r2.p2.y);

	//calcula o coeficiente angular das retas
	coeficiente1=((r1.p2.y - r1.p1.y) / (r1.p2.x - r1.p1.x));
	coeficiente2= ((r2.p2.y - r2.p1.y) / (r2.p2.x - r2.p1.x));


	//calcula o tamanho de cada cateto
	dx1 = r1.p2.x - r1.p1.x;//x da reta 1
	dy1 = r1.p2.y - r1.p1.y; //y da reta 1
	dx2 = r2.p2.x - r2.p1.x;//x da reta 2
	dy2 = r2.p2.y - r2.p1.y; //y da reta 2
	
	/*calcula o comprimento das retas;
	comprimento1=sqrt((pow((r1.p2.x - r1.p1.x),2)) + (pow((r1.p2.y - r1.p1.y),2)));
	comprimento2=sqrt((pow((r2.p2.x - r2.p1.x),2)) + (pow((r2.p2.y - r2.p1.y),2)));
	*/

	//calcula o comprimento das retas;
	comprimento1 = sqrt((pow(dx1,2)) + (pow(dy1,2)));
	comprimento2 = sqrt((pow(dx2,2)) + (pow(dy2,2)));
	
	
	//cria a equação das retas
	//y1 = coeficiente1*px1 + coeficiente1*r1.p1.x +r1.p1.y;//equação da reta 1
	//y2 = coeficiente2*px2 + coeficiente2*r2.p1.x +r2.p1.y;//equação da reta 2
	//y1 = coeficiente1*px1 + (r1.p1.y - coeficiente1*r1.p1.x);
	//iguala as equações para encontrar o x em comum

	//verifica se há intercessao entre as retas
	if (coeficiente1==coeficiente2)
	{
		flaginter = NAO_EXISTE_INTERCESSAO;
	}
	else
	{
		flaginter = EXISTE_INTERCESSAO;
	}

	//verifica se existe o coeficiente angular da reta 1
	if (r1.p2.x - r1.p1.x == 0)
	{
		flagcoef1 = COEFICIENTE_NAO_DEFINIDO;
	}
	else
	{
		flagcoef1 = COEFICIENTE_DEFINIDO;
	}

	if (flagcoef1==1)//definido
	{
		printf ("%.2f ",coeficiente1);
	}
	else
	{
		printf (MENSAGEM_COEFICIENTE_NAO_DEFINIDO);
		printf (" ");
	}


	//verifica se existe o coeficiente angular da reta 2
	if (r2.p2.x - r2.p1.x == 0)
	{
		flagcoef2 = COEFICIENTE_NAO_DEFINIDO;
	}
	else
	{
		flagcoef2 = COEFICIENTE_DEFINIDO;
	}

	if (flagcoef2==1)//definido
	{
		printf ("%.2f \n",coeficiente2);
	}
	else
	{
		printf (MENSAGEM_COEFICIENTE_NAO_DEFINIDO);
		printf ("\n");
	}

	printf ("%.2f ",comprimento1);
	printf ("%.2f\n",comprimento2);

	//printf ("coef 1: %f\n",coeficiente1);
	//printf ("coef 2: %f\n",coeficiente2);
	if (flaginter==1)
	{
		if (((r1.p1.x==r1.p2.x) && (r1.p1.y==r1.p2.y)) ||((r2.p1.x==r2.p2.x) && (r2.p1.y==r2.p2.y)))
		return 0;
		else
		{
		if (flagcoef2!=1 || flagcoef1!=1 )
		{//encontra o x e o y em comum das retas
		x = r1.p2.x - r1.p1.x;
		y = r1.p2.y - r1.p1.y;
		printf ("%.2f %.2f\n",x,y);}
		else
		{	
		//encontra o x e o y em comum das retas
		x = ((r2.p1.y - (coeficiente2*r2.p1.x)) - (r1.p1.y - (coeficiente1*r1.p1.x)))/(coeficiente1 - coeficiente2);
		y = coeficiente1*x + (r1.p1.y - (coeficiente1*r1.p1.x));
		printf ("%.2f %.2f\n",x,y);}
	}}
	

	return 0;
}
