/* ================================================================== *
    Universidade Federal de Sao Carlos - UFSCar, Sorocaba

    Disciplina: Algoritmos e Programação 1
    Prof. Tiago A. Almeida

    Lista 07 - Exercício 01 - K-Visinhos Mais Próximos

    Instrucoes
    ----------

    Este arquivo contem o codigo que auxiliara no desenvolvimento do
    exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
    Dados do aluno:

    RA: 587354
    Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>
#include <math.h>

//Estrutura para armazenar as coordenadas de um ponto.
/* <<< DEFINA AQUI A ESTRUTURA (Ponto) PARA ARMAZENAR CADA PONTO >>> */

typedef struct _ponto
{
	float x;
	float y;
}Ponto;

int main()
{
	Ponto pr, p1, p2, p3, p4, p5; //Estruturas para cada um dos pontos (p1,...,p5) e para o ponto de referência (pr).
	int k,i; //Número de vizinhos a serem considerados.
	float d1, d2, d3, d4, d5,trocadist,trocapontox,trocapontoy; //Armzenam as distâncias euclidianas entre cada ponto e pr.
    /* <<< DECLARE MAIS VARIAVEIS SE FOR NECESSARIO >>> */             

	scanf ("%f %f",&p1.x,&p1.y);
	scanf ("%f %f",&p2.x,&p2.y);
	scanf ("%f %f",&p3.x,&p3.y);
	scanf ("%f %f",&p4.x,&p4.y);
	scanf ("%f %f",&p5.x,&p5.y);
	scanf ("%f %f",&pr.x,&pr.y);
	//k tem que ser natural nao nulo	
	do
	{
		scanf ("%d",&k);
	}while (k<=0);
	
	d1 = (sqrt(pow((p1.x - pr.x),2) + (pow((p1.y - pr.y),2))));
	d2 = (sqrt(pow((p2.x - pr.x),2) + (pow((p2.y - pr.y),2))));
	d3 = (sqrt(pow((p3.x - pr.x),2) + (pow((p3.y - pr.y),2))));
	d4 = (sqrt(pow((p4.x - pr.x),2) + (pow((p4.y - pr.y),2))));
	d5 = (sqrt(pow((p5.x - pr.x),2) + (pow((p5.y - pr.y),2))));
	
	//apenas 5 valores
	if (k>5)
	{
		k=5;
	}
	
	//encontrar o menor valor e armazenar em d1	
	if (d2<d1)
	{
		//armazena a distancia euclidiana
		trocadist=d1;
		d1=d2;
		d2=trocadist;

		//armazena os pontos(x,y)

		//ponto X
		trocapontox=p1.x;
		p1.x=p2.x;
		p2.x=trocapontox;
		//ponto Y
		trocapontoy=p1.y;
		p1.y=p2.y;
		p2.y=trocapontoy;
	}

	if (d3<d1)
	{
		//armazena a distancia euclidiana
		trocadist=d1;
		d1=d3;
		d3=trocadist;

		//armazena os pontos(x,y)

		//ponto X
		trocapontox=p1.x;
		p1.x=p3.x;
		p3.x=trocapontox;
		//ponto Y
		trocapontoy=p1.y;
		p1.y=p3.y;
		p3.y=trocapontoy;
	}

	if (d4<d1)
	{
		//armazena a distancia euclidiana
		trocadist=d1;
		d1=d4;
		d4=trocadist;

		//armazena os pontos(x,y)

		//ponto X
		trocapontox=p1.x;
		p1.x=p4.x;
		p4.x=trocapontox;
		//ponto Y
		trocapontoy=p1.y;
		p1.y=p4.y;
		p4.y=trocapontoy;
	}

	if (d5<d1)
	{
		//armazena a distancia euclidiana
		trocadist=d1;
		d1=d5;
		d5=trocadist;

		//armazena os pontos(x,y)

		//ponto X
		trocapontox=p1.x;
		p1.x=p5.x;
		p5.x=trocapontox;
		//ponto Y
		trocapontoy=p1.y;
		p1.y=p5.y;
		p5.y=trocapontoy;
	}


	//encontrar o segundo menor valor e armazenar em d2	
	if (d1<d2)
	{
		//armazena a distancia euclidiana
		trocadist=d2;
		d2=d1;
		d1=trocadist;

		//armazena os pontos(x,y)

		//ponto X
		trocapontox=p2.x;
		p2.x=p1.x;
		p1.x=trocapontox;
		//ponto Y
		trocapontoy=p2.y;
		p2.y=p1.y;
		p1.y=trocapontoy;
	}

	if (d3<d2)
	{
		//armazena a distancia euclidiana
		trocadist=d2;
		d2=d3;
		d3=trocadist;

		//armazena os pontos(x,y)

		//ponto X
		trocapontox=p2.x;
		p2.x=p3.x;
		p3.x=trocapontox;
		//ponto Y
		trocapontoy=p2.y;
		p2.y=p3.y;
		p3.y=trocapontoy;
	}

	if (d4<d2)
	{
		//armazena a distancia euclidiana
		trocadist=d2;
		d2=d4;
		d4=trocadist;

		//armazena os pontos(x,y)

		//ponto X
		trocapontox=p2.x;
		p2.x=p4.x;
		p4.x=trocapontox;
		//ponto Y
		trocapontoy=p2.y;
		p2.y=p4.y;
		p4.y=trocapontoy;
	}

	if (d5<d2)
	{
		//armazena a distancia euclidiana
		trocadist=d2;
		d2=d5;
		d5=trocadist;

		//armazena os pontos(x,y)

		//ponto X
		trocapontox=p2.x;
		p2.x=p5.x;
		p5.x=trocapontox;
		//ponto Y
		trocapontoy=p2.y;
		p2.y=p5.y;
		p5.y=trocapontoy;
	}

	//encontrar o terceiro menor valor e armazenar em d3	
	if (d1<d3)
	{
		//armazena a distancia euclidiana
		trocadist=d3;
		d3=d1;
		d1=trocadist;

		//armazena os pontos(x,y)

		//ponto X
		trocapontox=p3.x;
		p3.x=p1.x;
		p1.x=trocapontox;
		//ponto Y
		trocapontoy=p3.y;
		p3.y=p1.y;
		p1.y=trocapontoy;
	}

	if (d2<d3)
	{
		//armazena a distancia euclidiana
		trocadist=d3;
		d3=d2;
		d2=trocadist;

		//armazena os pontos(x,y)

		//ponto X
		trocapontox=p3.x;
		p3.x=p2.x;
		p2.x=trocapontox;
		//ponto Y
		trocapontoy=p3.y;
		p3.y=p2.y;
		p2.y=trocapontoy;
	}

	if (d4<d3)
	{
		//armazena a distancia euclidiana
		trocadist=d3;
		d3=d4;
		d4=trocadist;

		//armazena os pontos(x,y)

		//ponto X
		trocapontox=p3.x;
		p3.x=p4.x;
		p4.x=trocapontox;
		//ponto Y
		trocapontoy=p3.y;
		p3.y=p4.y;
		p4.y=trocapontoy;
	}

	if (d5<d3)
	{
		//armazena a distancia euclidiana
		trocadist=d3;
		d3=d5;
		d5=trocadist;

		//armazena os pontos(x,y)

		//ponto X
		trocapontox=p3.x;
		p3.x=p5.x;
		p5.x=trocapontox;
		//ponto Y
		trocapontoy=p3.y;
		p3.y=p5.y;
		p5.y=trocapontoy;
	}

	//encontrar o quarto menor valor e armazenar em d4	
	if (d1<d4)
	{
		//armazena a distancia euclidiana
		trocadist=d4;
		d4=d1;
		d1=trocadist;

		//armazena os pontos(x,y)

		//ponto X
		trocapontox=p4.x;
		p4.x=p1.x;
		p1.x=trocapontox;
		//ponto Y
		trocapontoy=p4.y;
		p4.y=p1.y;
		p1.y=trocapontoy;
	}

	if (d2<d4)
	{
		//armazena a distancia euclidiana
		trocadist=d4;
		d4=d2;
		d2=trocadist;

		//armazena os pontos(x,y)

		//ponto X
		trocapontox=p4.x;
		p4.x=p2.x;
		p2.x=trocapontox;
		//ponto Y
		trocapontoy=p4.y;
		p4.y=p2.y;
		p2.y=trocapontoy;
	}

	if (d3<d4)
	{
		//armazena a distancia euclidiana
		trocadist=d4;
		d4=d3;
		d3=trocadist;

		//armazena os pontos(x,y)

		//ponto X
		trocapontox=p4.x;
		p4.x=p3.x;
		p3.x=trocapontox;
		//ponto Y
		trocapontoy=p4.y;
		p4.y=p3.y;
		p3.y=trocapontoy;
	}

	if (d5<d4)
	{
		//armazena a distancia euclidiana
		trocadist=d4;
		d4=d5;
		d5=trocadist;

		//armazena os pontos(x,y)

		//ponto X
		trocapontox=p4.x;
		p4.x=p5.x;
		p5.x=trocapontox;
		//ponto Y
		trocapontoy=p4.y;
		p4.y=p5.y;
		p5.y=trocapontoy;
	}

	//encontrar o quinto menor valor e armazenar em d5	
	if (d1<d5)
	{
		//armazena a distancia euclidiana
		trocadist=d5;
		d5=d1;
		d1=trocadist;

		//armazena os pontos(x,y)

		//ponto X
		trocapontox=p5.x;
		p5.x=p1.x;
		p1.x=trocapontox;
		//ponto Y
		trocapontoy=p5.y;
		p5.y=p1.y;
		p1.y=trocapontoy;
	}

	if (d2<d5)
	{
		//armazena a distancia euclidiana
		trocadist=d5;
		d5=d2;
		d2=trocadist;

		//armazena os pontos(x,y)

		//ponto X
		trocapontox=p5.x;
		p5.x=p2.x;
		p2.x=trocapontox;
		//ponto Y
		trocapontoy=p5.y;
		p5.y=p2.y;
		p2.y=trocapontoy;
	}

	if (d3<d5)
	{
		//armazena a distancia euclidiana
		trocadist=d5;
		d5=d3;
		d3=trocadist;

		//armazena os pontos(x,y)

		//ponto X
		trocapontox=p5.x;
		p5.x=p3.x;
		p3.x=trocapontox;
		//ponto Y
		trocapontoy=p5.y;
		p5.y=p3.y;
		p3.y=trocapontoy;
	}

	if (d4<d5)
	{
		//armazena a distancia euclidiana
		trocadist=d5;
		d5=d4;
		d4=trocadist;

		//armazena os pontos(x,y)

		//ponto X
		trocapontox=p5.x;
		p5.x=p4.x;
		p4.x=trocapontox;
		//ponto Y
		trocapontoy=p5.y;
		p5.y=p4.y;
		p4.y=trocapontoy;
	}


	//imprime os k vizinhos mais proximos
	if (k==1)
	{
		printf ("%.2f %.2f\n",p5.x,p5.y);//coordenada
		printf("%.2f\n",d5);//distancia
	}

	if (k==2)
	{
		printf ("%.2f %.2f\n",p5.x,p5.y);//coordenada
		printf("%.2f\n",d5);//distancia
		printf ("%.2f %.2f\n",p4.x,p4.y);//coordenada
		printf("%.2f\n",d4);//distancia	
	}

	if (k==3)
	{
		printf ("%.2f %.2f\n",p5.x,p5.y);//coordenada
		printf("%.2f\n",d5);//distancia
		printf ("%.2f %.2f\n",p4.x,p4.y);//coordenada
		printf("%.2f\n",d4);//distancia
		printf ("%.2f %.2f\n",p3.x,p3.y);//coordenada
		printf("%.2f\n",d3);//distancia
	}

	if (k==4)
	{
		printf ("%.2f %.2f\n",p5.x,p5.y);//coordenada
		printf("%.2f\n",d5);//distancia
		printf ("%.2f %.2f\n",p4.x,p4.y);//coordenada
		printf("%.2f\n",d4);//distancia
		printf ("%.2f %.2f\n",p3.x,p3.y);//coordenada
		printf("%.2f\n",d3);//distancia
		printf ("%.2f %.2f\n",p2.x,p2.y);//coordenada
		printf("%.2f\n",d2);//distancia
	}

	if (k==5)
	{
		printf ("%.2f %.2f\n",p5.x,p5.y);//coordenada
		printf("%.2f\n",d5);//distancia
		printf ("%.2f %.2f\n",p4.x,p4.y);//coordenada
		printf("%.2f\n",d4);//distancia
		printf ("%.2f %.2f\n",p3.x,p3.y);//coordenada
		printf("%.2f\n",d3);//distancia
		printf ("%.2f %.2f\n",p2.x,p2.y);//coordenada
		printf("%.2f\n",d2);//distancia
		printf ("%.2f %.2f\n",p1.x,p1.y);//coordenada
		printf("%.2f\n",d1);//distancia	
	}
	return 0;
}
