/* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 1
	Prof. Tiago A. Almeida

	Lista 07 - Exercício 04 - Polinomios

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354
	Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h> 
#include <math.h>

#define SAIR      0
#define SOMA      1
#define SUBTRACAO 2
#define RESOLVER  3

#define NAO_EXISTE_RAIZ "nao existem raizes\n"

//Estrutura que corresponde a um polinômio no formato ax² + bx + c.
/* <<< DEFINA AQUI A ESTRUTURA (Polinomio) PARA ARMAZENAR OS COEFICIENTES DA EQUACAO >>> */
typedef struct _polinomio
{
	int a;
	int b;
	int c;
}Polinomio;

/* ================================================================== */
/* ======================== ROTINA PRINCIPAL ======================== */
/* ================================================================== */

int main()
{
	int opcao;
	Polinomio Px, Qx, Rx; // Polinomios de entrada (P e Q) e de saída (R)
	/* <<< DECLARE MAIS VARIAVEIS SE FOR NECESSARIO >>> */
	float delta,raiz1,raiz2;

	scanf("%d", &opcao);

	while (opcao != SAIR)
	{
		switch (opcao)
		{
			case SOMA:
				scanf ("%d %d %d",&Px.a,&Px.b,&Px.c);
				scanf ("%d %d %d",&Qx.a,&Qx.b,&Qx.c);	
				Rx.a = Px.a + Qx.a;
				Rx.b = Px.b + Qx.b;
				Rx.c = Px.c + Qx.c;
				if (Rx.b>0 && Rx.c>0)
					printf ("%dx2+%dx+%d\n",Rx.a,Rx.b,Rx.c);
				else
				{
					if (Rx.b>0 && Rx.c==0)
						printf ("%dx2+%dx+%d\n",Rx.a,Rx.b,Rx.c);
					if (Rx.b<0 && Rx.c==0)
						printf ("%dx2%dx+%d\n",Rx.a,Rx.b,Rx.c);
					if (Rx.b>0 && Rx.c<0)
						printf ("%dx2+%dx%d\n",Rx.a,Rx.b,Rx.c);
					if (Rx.c>0 && Rx.b==0)
						printf ("%dx2+%dx+%d\n",Rx.a,Rx.b,Rx.c);
					if (Rx.c<0 && Rx.b==0)
						printf ("%dx2+%dx%d\n",Rx.a,Rx.b,Rx.c);
					if (Rx.c>0 && Rx.b<0)
						printf ("%dx2%dx+%d\n",Rx.a,Rx.b,Rx.c);
					if (Rx.c<0 && Rx.b<0)
						printf ("%dx2%dx%d\n",Rx.a,Rx.b,Rx.c);
					if (Rx.b==0 && Rx.c==0)
						printf ("%dx2+%dx+%d\n",Rx.a,Rx.b,Rx.c);
					/*else
					printf ("%dx2%dx%d\n",Rx.a,Rx.b,Rx.c);*/
				}
			break;
				

			case SUBTRACAO:
				scanf ("%d %d %d",&Px.a,&Px.b,&Px.c);
				scanf ("%d %d %d",&Qx.a,&Qx.b,&Qx.c);
				Rx.a = Px.a - Qx.a;
				Rx.b = Px.b - Qx.b;
				Rx.c = Px.c - Qx.c;
				if (Rx.b>0 && Rx.c>0)
					printf ("%dx2+%dx+%d\n",Rx.a,Rx.b,Rx.c);
				else
				{
					if (Rx.b>0 && Rx.c==0)
						printf ("%dx2+%dx+%d\n",Rx.a,Rx.b,Rx.c);
					if (Rx.b<0 && Rx.c==0)
						printf ("%dx2%dx+%d\n",Rx.a,Rx.b,Rx.c);
					if (Rx.b>0 && Rx.c<0)
						printf ("%dx2+%dx%d\n",Rx.a,Rx.b,Rx.c);
					if (Rx.c>0 && Rx.b==0)
						printf ("%dx2+%dx+%d\n",Rx.a,Rx.b,Rx.c);
					if (Rx.c<0 && Rx.b==0)
						printf ("%dx2+%dx%d\n",Rx.a,Rx.b,Rx.c);
					if (Rx.c>0 && Rx.b<0)
						printf ("%dx2%dx+%d\n",Rx.a,Rx.b,Rx.c);
					if (Rx.c<0 && Rx.b<0)
						printf ("%dx2%dx%d\n",Rx.a,Rx.b,Rx.c);
					if (Rx.b==0 && Rx.c==0)
						printf ("%dx2+%dx+%d\n",Rx.a,Rx.b,Rx.c);
					/*else
					printf ("%dx2%dx%d\n",Rx.a,Rx.b,Rx.c);*/
				}			
				
			break;


			case RESOLVER:
				scanf ("%d %d %d",&Px.a,&Px.b,&Px.c);
				delta = pow(Px.b,2) - 4*Px.a*Px.c;
				if (delta<0)
				{
					printf (NAO_EXISTE_RAIZ);
				}
				else
				{
					if (delta==0)
					{
						raiz1=((- Px.b) + sqrt(delta))/(2*Px.a);

						if (raiz1==-0)
						{
							raiz1=0;
						}
						printf ("%.2f\n",raiz1);
					}
					else
					{
						raiz1=((- Px.b) + sqrt(delta))/(2*Px.a);
						raiz2=((- Px.b) - sqrt(delta))/(2*Px.a);

						if (raiz1>raiz2)
						{
							printf ("%.2f\n",raiz1);
							printf ("%.2f\n",raiz2);
						}
						else
						{
							printf ("%.2f\n",raiz2);
							printf ("%.2f\n",raiz1);
						}
						
					}
				}
			break;

		}

		scanf("%d", &opcao);
	}

	return (0);
}
