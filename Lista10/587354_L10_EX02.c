/* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 1
	Prof. Tiago A. Almeida

	Lista 10 - Exercício 02 - Funcoes e Procedimentos com Vetores

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354
	Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>
#include <math.h>


#define MAX 100 //Tamanho máximo do vetor.


//Enumeração das opções do menu.
enum opcoes{
	OPC_SAIR,
	OPC_TAMANHOVETOR,
	OPC_POPULARVETOR,
	OPC_IMPRIMIR,
	OPC_IMPRIMIRINVERSO,
	OPC_SOMA,
	OPC_SOMAPOSITIVOS,
	OPC_SOMANEGATIVOS,
	OPC_MAXIMO,
	OPC_MINIMO,
	OPC_MEDIA,
	OPC_MEDIAHARMONICA,
	OPC_DESVIOPADRAO,
	OPC_MEDIANA,
	OPC_ENCONTRARPRIMEIRO,
	OPC_ENCONTRARULTIMO,
	OPC_CONTAR,
	OPC_ORDENARCRESCENTE,
	OPC_ORDENARDECRESCENTE
};


/* ---------------------- */
/* DECLARACAO DAS FUNCOES */
/* ---------------------- */

int tamanhoVetor();
void popularVetor(float vetor[], int tamanho);
void imprimirVetor(float vetor[], int numElementos);
void imprimirInverso(float vetor[], int numElementos);
float soma(float vetor[], int numElementos);
float somaPositivos(float vetor[], int numElementos);
float somaNegativos(float vetor[],int numElementos);
float maximo(float vetor[], int numElementos);
float minimo(float vetor[], int numElementos);
float media(float vetor[], int numElementos);
float mediaHarmonica(float vetor[], int numElementos);
float desvioPadrao(float vetor[], int numElementos);
float mediana(float vetor[], int numElementos);
int encontrarPrimeiro(float numero, float vetor[], int numElementos);
int encontrarUltimo(float numero, float vetor[], int numElementos);
int contar(float numero, float vetor[], int numElementos); 
float ordenarCrescente(float vetor[], int numElementos);
float ordenarDecrescente(float vetor[], int numElementos);
/* ------------------------------ */
/* ROTINA PRINCIPAL - NÃO ALTERAR */
/* ------------------------------ */
int main(){
	int numElementos = 0,	//Armazena o tamanho do vetor.
		opcao;				//Armazena o a opção selecionada pelo usuário.
	float vetor[MAX],
		  numero;			//Armazena o número digitado pelo usuário para contagem e busca.

	scanf("%d", &opcao);	//Lê a opção selecionada pelo usuário.

	//Executa as opções enquanto o usuário não digitar a opção 0 - Sair.
	while(opcao != OPC_SAIR){

		switch(opcao){			

		//----------------------------------------------------------
		case OPC_TAMANHOVETOR:
			numElementos = tamanhoVetor();
		break;
		//----------------------------------------------------------

		//----------------------------------------------------------
		case OPC_POPULARVETOR:
			popularVetor(vetor, numElementos);
		break;
		//----------------------------------------------------------

		//----------------------------------------------------------
		case OPC_IMPRIMIR:
			imprimirVetor(vetor, numElementos);
		break;
		//----------------------------------------------------------

		//----------------------------------------------------------
		case OPC_IMPRIMIRINVERSO:
			imprimirInverso(vetor, numElementos);
		break;
		//----------------------------------------------------------

		//----------------------------------------------------------
		case OPC_SOMA:
			printf("%.2f\n", soma(vetor, numElementos));
		break;
		//----------------------------------------------------------

		//----------------------------------------------------------
		case OPC_SOMAPOSITIVOS:
			printf("%.2f\n", somaPositivos(vetor, numElementos));
		break;
		//----------------------------------------------------------

		//----------------------------------------------------------
		case OPC_SOMANEGATIVOS:
			printf("%.2f\n", somaNegativos(vetor, numElementos));
		break;
		//----------------------------------------------------------

		//----------------------------------------------------------
		case OPC_MAXIMO:
			printf("%.2f\n", maximo(vetor, numElementos));
		break;
		//----------------------------------------------------------

		//----------------------------------------------------------
		case OPC_MINIMO:
			printf("%.2f\n", minimo(vetor, numElementos));
		break;
		//----------------------------------------------------------

		//----------------------------------------------------------
		case OPC_MEDIA:
			printf("%.2f\n", media(vetor, numElementos));
		break;
		//----------------------------------------------------------

		//----------------------------------------------------------
		case OPC_MEDIAHARMONICA:
			printf("%.2f\n", mediaHarmonica(vetor, numElementos));
		break;
		//----------------------------------------------------------

		//----------------------------------------------------------
		case OPC_DESVIOPADRAO:
			printf("%.2f\n", desvioPadrao(vetor, numElementos));
		break;
		//----------------------------------------------------------

		//----------------------------------------------------------
		case OPC_MEDIANA:
			printf("%.2f\n", mediana(vetor, numElementos));
		break;
		//----------------------------------------------------------

		//----------------------------------------------------------
		case OPC_ENCONTRARPRIMEIRO:
			scanf("%f", &numero);
			printf("%d\n", encontrarPrimeiro(numero, vetor, numElementos));
		break;
		//----------------------------------------------------------

		//----------------------------------------------------------
		case OPC_ENCONTRARULTIMO:
			scanf("%f", &numero);
			printf("%d\n", encontrarUltimo(numero, vetor, numElementos));
		break;
		//----------------------------------------------------------

		//----------------------------------------------------------
		case OPC_CONTAR:
			scanf("%f", &numero);
			printf("%d\n", contar(numero, vetor, numElementos));
		break;
		//----------------------------------------------------------

		//----------------------------------------------------------
		case OPC_ORDENARCRESCENTE:
			ordenarCrescente(vetor, numElementos);
		break;
		//----------------------------------------------------------

		//----------------------------------------------------------
		case OPC_ORDENARDECRESCENTE:
			ordenarDecrescente(vetor, numElementos);
		break;
		//----------------------------------------------------------
		}

		scanf("%d", &opcao);		
	}

	return (0);
}



/* ------------------------- */
/* IMPLEMENTACAO DAS FUNCOES */
/* ------------------------- */

//Lê o tamanho do vetor passado pelo usuário e retorna o valor.
int tamanhoVetor(){
	int tamanho;

	do {
		scanf("%d", &tamanho);
	} while(tamanho < 1 || tamanho > 100);

	return (tamanho);
}


//Lê os elementos do vetor passados pelo usuário.
void popularVetor(float vetor[], int tamanho){
	int i;

	for(i = 0; i < tamanho; i++)
		scanf("%f", &vetor[i]);
}
void imprimirVetor(float vetor[], int numElementos)
{
	int i;
	for (i=0;i<numElementos;i++)
	{
		printf ("%.2f ",vetor[i]);
	}
	printf ("\n");
}
void imprimirInverso(float vetor[], int numElementos)
{
	int i;
	for (i=numElementos-1;i>=0;i--)
	{
		printf ("%.2f ",vetor[i]);
	}
	printf ("\n");
}
float soma(float vetor[], int numElementos)
{
	int i,soma = 0;
	for (i=0;i<numElementos;i++)
	{
		soma = soma + vetor[i];
	}
	return(soma);
}
float somaPositivos(float vetor[], int numElementos)
{
	int i,soma = 0;
	for (i=0;i<numElementos;i++)
	{
		if (vetor[i]>0)
			soma = soma + vetor[i];
	}
	return(soma);
}
float somaNegativos(float vetor[],int numElementos)
{
	int i,soma = 0;
	for (i=0;i<numElementos;i++)
	{
		if (vetor[i]<0)
			soma = soma + vetor[i];
	}
	return(soma);
}
float maximo(float vetor[], int numElementos)
{
	int i,maior =vetor[0];
	for (i=0;i<numElementos;i++)
	{
		if (vetor[i]>maior)
			maior = vetor[i];
	}
	return(maior);
}
float minimo(float vetor[], int numElementos)
{
	int i,menor =vetor[0];
	for (i=0;i<numElementos;i++)
	{
		if (vetor[i]<menor)
			menor = vetor[i];
	}
	return(menor);
}
float media(float vetor[], int numElementos)
{
	int i;
	float media = 0;
	for (i=0;i<numElementos;i++)
	{
		media = media + vetor[i];
	}
	media = media/numElementos;
	return(media);
}
float mediaHarmonica(float vetor[], int numElementos)
{
	int i;
	float media = 0;
	for (i=0;i<numElementos;i++)
	{
		media = media + (1/vetor[i]);
	}
	media = numElementos/media;
	return(media);
}
float desvioPadrao(float vetor[], int numElementos)
{
	//float mediadp = media(vetor, numElementos);
	int i;
	float media = 0, aux = 0,dp;
	for (i=0;i<numElementos;i++)
	{
		media = media + vetor[i];
	}
	media = media/numElementos;
	
	for (i=0;i<numElementos;i++)
	{
		aux = pow((vetor[i]-media),2) + aux;
	}
	aux = aux/(numElementos-1);
	dp = sqrt(aux);
	return(dp);
}
float mediana(float vetor[], int numElementos)
{
	int i; 
        float m1, m2;
    
  	ordenarCrescente (vetor, numElementos); // Ordenando conjunto numerico.
 
	  switch (numElementos % 2) // Seletor para calculo da mediana.
	  {
	    case 0: // Faixa de valores (qtd de elem do vetor) e PAR.
	      m1 = vetor[numElementos / 2 - 1];
	      m2 = vetor[numElementos / 2];    
	      m1 += m2;
	      return  (m1 / 2);    
	     
	    case 1: // Faixa de valores do vetor e IMPAR.
	      m1 = vetor[ (numElementos - 1) / 2 ];
	      return (m1);  
	  }  
}
int encontrarPrimeiro(float numero, float vetor[], int numElementos)
{
	int i, flag = 1;
	float indice;
	for (i=0;i<numElementos && flag==1;i++)//para ao achar o primeiro numero
	{
		if (numero == vetor[i])
		{
			indice = i;
			flag = 0;
		}
	}
	if (flag==1)//nao tem o numero no vetor
		indice = -1;
	return (indice);
}
int encontrarUltimo(float numero, float vetor[], int numElementos)
{
	int i, flag = 1;
	float indice;
	for (i=0;i<numElementos;i++)
	{
		if (numero == vetor[i])
		{
			indice = i;
			flag = 0;
		}
	}
	if (flag==1)//nao tem o numero no vetor
		indice = -1;
	return (indice);
}
int contar(float numero, float vetor[], int numElementos)
{
	int i,soma=0;
	for (i=0;i<numElementos;i++)
	{
		if (numero == vetor[i])
		{
			soma++;
		}
	}
	return (soma);
}
float ordenarCrescente(float vetor[], int numElementos)//Metodo Bolha
{
      float aux;
      int j, i;
       
      for(i = 0; i < numElementos-1; i++)
      {
         for(j=i+1; j < numElementos ; j++)
	 {
            if(vetor[i] > vetor[j])
	    {
               aux = vetor[i];
               vetor[i] = vetor[j];
               vetor[j] = aux;
            }
         }
      }  
	/*for (i=0;i<numElementos;i++)
	{
		printf ("%.2f ",vetor[i]);
	}
 	printf ("\n");*/
}
float ordenarDecrescente(float vetor[], int numElementos)//Metodo Bolha
{
	 float aux;
      int j, i;
       
      for(i = 0; i < numElementos-1; i++)
      {
         for(j=i+1; j < numElementos ; j++)
	 {
            if(vetor[i] < vetor[j])
	    {
               aux = vetor[i];
               vetor[i] = vetor[j];
               vetor[j] = aux;
            }
         }
      }  
	/*for (i=0;i<numElementos;i++)
	{
		printf ("%.2f ",vetor[i]);
	}
 	printf ("\n");*/
}
