/* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 1
	Prof. Tiago A. Almeida

	Lista 10 - Exercício 01 - Funcoes e Procedimentos

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354
	Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>
#include <math.h>


// Enumeração das opções do menu.
enum opcoes{
	OPC_SAIR,
	OPC_FIBONACCI_1,
	OPC_FIBONACCI_2,
	OPC_FATORIAL_1,
	OPC_FATORIAL_2,
	OPC_POTENCIA,
	OPC_SOMAPARES,
	OPC_SOMAIMPARES,
	OPC_SOMAPRIMOS,
	OPC_MAIORPRIMO,
	OPC_MMC,
	OPC_MDC,
	OPC_DESVIOPADRAO,
	OPC_COMBINACAO,
	OPC_MEDIAHARMONICA,
	OPC_COEFICIENTEVARIACAO
};


/* ---------------------- */
/* DECLARACAO DAS FUNCOES */
/* ---------------------- */

int fibonacci (int n);
int fatorial (int n);
float potencia (int n1, int n2);
int somaDosPares (int n1, int n2);
int somaDosImpares (int n1, int n2);
int somaDosPrimos (int n1, int n2);
int maiorPrimo (int n1, int n2);
int mmc (int n1, int n2);
int mdc (int n1, int n2);
float desvioPadrao (int n1, int n2);
float combinacao (int n1, int n2); 
float mediaHarmonica (int n1, int n2);
float coeficienteVariacao (int n1, int n2);
/* ------------------------------ */
/* ROTINA PRINCIPAL - NÃO ALTERAR */
/* ------------------------------ */
int main(){
	int n1, n2, //Armazenam os números n1 e n2 para os cálculos.
		opc; 	//Armazena a última opção selecionada pelo usuário.

	scanf("%d %d", &n1, &n2); //Lê os valores n1 e n2.
	scanf("%d", &opc);		  //Lê a opção do usuário.

	//Repete enquanto o usuário não selecionar a opção 0 - Sair.
	while(opc != OPC_SAIR){

		//Imprime o resultado de acordo com a opção selecionada.
		switch(opc){
			//-----------------------------------------------------------
			case OPC_FIBONACCI_1:
				printf("%d\n", fibonacci(n1));
			break;
			//-----------------------------------------------------------

			//-----------------------------------------------------------
			case OPC_FIBONACCI_2:
				printf("%d\n", fibonacci(n2));
			break;
			//-----------------------------------------------------------

			//-----------------------------------------------------------
			case OPC_FATORIAL_1:
				printf("%d\n", fatorial(n1));
			break;
			//-----------------------------------------------------------

			//-----------------------------------------------------------
			case OPC_FATORIAL_2:
				printf("%d\n", fatorial(n2));
			break;
			//-----------------------------------------------------------

			//-----------------------------------------------------------
			case OPC_POTENCIA:
				printf("%.0f\n", potencia(n1, n2));
			break;
			//-----------------------------------------------------------

			//-----------------------------------------------------------
			case OPC_SOMAPARES:
				printf("%d\n", somaDosPares(n1, n2));
			break;
			//-----------------------------------------------------------

			//-----------------------------------------------------------
			case OPC_SOMAIMPARES:
				printf("%d\n", somaDosImpares(n1, n2));
			break;
			//-----------------------------------------------------------

			//-----------------------------------------------------------
			case OPC_SOMAPRIMOS:
				printf("%d\n", somaDosPrimos(n1, n2));
			break;
			//-----------------------------------------------------------

			//-----------------------------------------------------------
			case OPC_MAIORPRIMO:
				printf("%d\n", maiorPrimo(n1, n2));
			break;
			//-----------------------------------------------------------

			//-----------------------------------------------------------
			case OPC_MMC:
				printf("%d\n", mmc(n1, n2));
			break;
			//-----------------------------------------------------------

			//-----------------------------------------------------------
			case OPC_MDC:
				printf("%d\n", mdc(n1, n2));
			break;
			//-----------------------------------------------------------

			//-----------------------------------------------------------
			case OPC_DESVIOPADRAO:
				printf("%.2f\n", desvioPadrao(n1, n2));
			break;
			//-----------------------------------------------------------

			//-----------------------------------------------------------
			case OPC_COMBINACAO:
				printf("%.2f\n", combinacao(n1, n2));
			break;
			//-----------------------------------------------------------

			//-----------------------------------------------------------
			case OPC_MEDIAHARMONICA:
				printf("%.2f\n", mediaHarmonica(n1, n2));
			break;
			//-----------------------------------------------------------

			//-----------------------------------------------------------
			case OPC_COEFICIENTEVARIACAO:
				printf("%.2f\n", coeficienteVariacao(n1, n2));
			break;
			//-----------------------------------------------------------
		}

		scanf("%d", &opc); //Lê a opção do usuário.

	}

	//Finaliza o programa.
	return (0);
}


/* ------------------------- */
/* IMPLEMENTACAO DAS FUNCOES */
/* ------------------------- */

int fibonacci (int n)
{	int soma = 0,
	    aux2=0,
	    aux1=1,
	    i;
	for (i=0;i<n;i++)
	{
		aux2 = aux1;
		aux1 = soma;
		soma = aux1 + aux2;
	}
	return (soma);
}
int fatorial (int n)
{
	int i,soma=1;
	for (i=n;i>0;i--)
	{
		soma = soma*i;
	}
	return (soma);
}
float potencia (int n1, int n2)
{
	int i,
	    result = 1;
	for (i=1;i<=n2;i++)
	{
		result = result * n1;
	}
	return (result);
}
int somaDosPares (int n1, int n2)
{
	int soma = 0,i;
	for (i=n1;i<=n2;i++)
	{
		if (i % 2 == 0)
			soma = soma + i;
	}
	return (soma);
}
int somaDosImpares (int n1, int n2)
{
	int soma = 0,i;
	for (i=n1;i<=n2;i++)
	{
		if (i % 2 != 0)
			soma = soma + i;
	}
	return (soma);
}
int somaDosPrimos (int n1, int n2)
{
	int soma = 0,i,primo = 1,j;
	if (n1<=n2)
	{
		for (i=n1;i<=n2;i++)
		{
			primo = 1;//reseta a variavael a cada execução do for
			for (j=2;j<i && primo==1;j++)//verifica se o numero do intervalo é primo
			{
				if (i % j== 0)
					primo = 0;
			}
			if (primo == 1 && i!=1)//se for primo soma e não for o numero 1
				soma = soma + i;
		}
	}
	else
	{
		for (i=n2;i<=n1;i++)
		{
			primo = 1;//reseta a variavael a cada execução do for
			for (j=2;j<i && primo==1;j++)//verifica se o numero do intervalo é primo
			{
				if (i % j== 0)
					primo = 0;
			}
			if (primo == 1 && i!=1)//se for primo soma e não for o numero 1
				soma = soma + i;
		}
	}
	return (soma);
}
int maiorPrimo (int n1, int n2)
{
	int maior = n1,i,primo = 1,j,cont=0;
	if (n1<=n2)
	{
		for (i=n1;i<=n2;i++)
		{
			primo = 1;//reseta a variavael a cada execução do for
			for (j=2;j<i && primo==1;j++)//verifica se o numero do intervalo é primo
			{
				if (i % j== 0)
					primo = 0;
			}
			if (primo == 1 && i!=1)//so numero é primo
			{	cont++;	
				if (i>maior)//verifica se é maior 
					maior = i;
			}
		}
	}
	else
	{ 	maior = n2;
		cont=0;
		for (i=n2;i<=n1;i++)
		{
			primo = 1;//reseta a variavael a cada execução do for
			for (j=2;j<i && primo==1;j++)//verifica se o numero do intervalo é primo
			{
				if (i % j== 0)
					primo = 0;
			}
			if (primo == 1 && i!=1)//so numero é primo
			{	cont++;	
				if (i>maior)//verifica se é maior 
					maior = i;
			}
		}
	}
	if (cont==0)//não tem numero primo primo no intervalo
		maior = 0;
	return (maior);
}
int mmc (int n1, int n2)
{
	int divisivel = 0,i;
	if (n1==0 || n2==0)
		return 0;
	else
	if (n1<=n2)
	{
		for (i=n1;divisivel!=1;i++)
		{	
			if (((i % n1 == 0) && (i % n2 == 0)))
				divisivel = 1;
		}
	}
	else
	{
		for (i=n2;divisivel!=1;i++)
		{	
			if (((i % n1 == 0) && (i % n2 == 0)))
				divisivel = 1;
		}
	}
	return (i-1);
}
int mdc (int n1, int n2)
{
	int resto,num = n1,denom = n2;
	if (n1==0 || n2==0)
		
		return (0);
	else
	{
		while (resto!=0)//metódo das divisões sucessivas (http://www.somatematica.com.br/fundam/mdc.php)
		{
			resto = num % denom; 	
			num = denom;
			denom = resto;
		}
		return(num);
	}
}
float desvioPadrao (int n1, int n2) //http://educacao.uol.com.br/disciplinas/matematica/media-desvio-padrao-e-variancia-nocoes-de-estatistica.htm
{
	float media = 0,i,cont=0,aux=0,dp=0;
	if (n1<=n2)
	{
		for (i=n1;i<=n2;i++)
		{
			media = media + i;
			cont++;
		}
		media = media/cont;
		for (i=n1;i<=n2;i++)
		{
			aux = pow((i-media),2) + aux;
		}
	}
	else
	{
		for (i=n2;i<=n1;i++)
		{
			media = media + i;
			cont++;
		}
		media = media/cont;
		for (i=n2;i<=n1;i++)
		{
			aux = pow((i-media),2) + aux;
		}
	}
	aux = aux/(cont-1);
	dp = sqrt(aux);
	
	return (dp);
}
float combinacao (int n1, int n2)
{
	int n,p,np,fatnp;
	float result;
	np = n1 - n2;
	if (n1<n2)
		result=0;
	else
	{
		if (np==0)
			np = 1;//zero fatorial é 1
		n = fatorial(n1);
		p = fatorial (n2);
		fatnp = fatorial (np);
		result = n/(p*fatnp);
	}
	return (result);
}
float mediaHarmonica (int n1, int n2)
{
	float media = 0,i,cont=0;
	if (n1<=n2)
	{
		for (i=n1;i<=n2;i++)
		{
			media = media + (1/i);
			cont++;
		}
	}
	else
	{
		for (i=n2;i<=n1;i++)
		{
			media = media + (1/i);
			cont++;
		}
	}
	media = cont/media;
	return (media);
}
float coeficienteVariacao (int n1, int n2)
{
	float media = 0,i,cont=0,cf,dp;
	dp = desvioPadrao (n1,n2);
	if (n1<=n2)
	{
		for (i=n1;i<=n2;i++)
		{
			media = media + i;
			cont++;
		}
	}
	else
	{
		for (i=n2;i<=n1;i++)
		{
			media = media + i;
			cont++;
		}
	}
	media = media/cont;
	cf = dp/media;
	return (cf);
}
