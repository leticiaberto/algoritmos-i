/*Exercicio 8 da lista 2
587354 - 02/04/2014*/

#include <stdio.h>
int main()
{
	float deposito, taxa_juros, val_rend, total;

	scanf ("%f", &deposito);
	scanf ("%f", &taxa_juros);

	val_rend = (deposito * taxa_juros)/100;
	total = deposito + val_rend;

	printf ("%.2f",val_rend);
	printf ("\n%.2f",total);
	
	return (0);
}
