/*Exercicio 11 da lista 2
587354 - 03/04/2014*/

#include <stdio.h>
int main ()
{
	int num, cubo, quad;
	cubo = 0;
	quad = 0;
	scanf ("%d", &num);
	if (num <= 0) 
		main();
	else
	{
		quad = num * num;
		cubo = num * num * num;
		printf ("%d", quad);
		printf ("\n%d", cubo);
	}
	
	return (0);
}
