/* Exercicio 13 da lista 2
587354 - 03/02/2014*/

#include <stdio.h>
int main ()
{
	int ano_atual, ano_nasc, idade, idade_futu;

	scanf ("%d", &ano_nasc);
	scanf ("%d", &ano_atual);

	idade = ano_atual - ano_nasc;
	idade_futu = 2050 - ano_nasc;

	printf ("%d", idade);
	printf ("\n%d", idade_futu);
	return (0);
}
