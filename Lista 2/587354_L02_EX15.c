/* Exercicio 13 da lista 02
587354 - 03/02/2014*/

#include <stdio.h>
int main ()
{
	float numhoras_trab, sal_min, sal_recebe,val_horas,sal_bruto,imposto;

	scanf ("%f", &numhoras_trab);
	scanf ("%f", &sal_min);

	val_horas = sal_min / 2;
	sal_bruto = numhoras_trab * val_horas;
	imposto = (sal_bruto * 3)/100;
	sal_recebe = sal_bruto - imposto;

	printf ("%.2f", sal_recebe);
	return (0);
}
