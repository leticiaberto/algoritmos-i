/*Exercicio 12 da lista 2
587354 - 03/04/2014*/

#include <stdio.h>
int main ()
{
	int pes, polegadas, jardas, milhas;

	scanf ("%d", &pes);

	polegadas = pes * 12;
	jardas =  3 * pes;
	milhas = 1760 * jardas;

	printf("%d", polegadas);
	printf ("\n%d", jardas);
	printf ("\n%d", milhas);

	return (0);
}
