/* Exercicio 14 da lista 2
587354 - 03/04/2014*/

#include <stdio.h>
int main()
{
	float preco_fabri, perc_lucro, perc_impos, total,lucro,imposto;

	scanf ("%f", &preco_fabri);
	scanf ("%f", &perc_lucro);
	scanf ("%f", &perc_impos);

	lucro = (preco_fabri * perc_lucro)/100;
	imposto = (preco_fabri * perc_impos)/100;
	total = preco_fabri + lucro + imposto;
	
	printf ("%.2f",lucro);
	printf ("\n%.2f",imposto);
	printf ("\n%.2f",total);
	return (0);
}
