/* ================================================================== *
  Universidade Federal de Sao Carlos - UFSCar, Sorocaba

  Disciplina: Algoritmos e Programação 1
  Prof. Tiago A. Almeida

  Lista 06 - Exercício 05 - Binário e Decimal

  Instrucoes
  ----------

  Este arquivo contem o codigo que auxiliara no desenvolvimento do
  exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
  Dados do aluno:

  RA: 587354
  Nome: Leticia Mara Berto

* ================================================================== */

#include <stdio.h>
#include <math.h>

#define SAIR                0
#define OPC_BINARIO_DECIMAL 1
#define OPC_DECIMAL_BINARIO 2

/* ================================================================== */
/* ======================== ROTINA PRINCIPAL ======================== */
/* ================================================================== */

int main()
{
  	int op=3,qntddigbin,bin,contbin,numdec,base2,basebin,decimal,potdec,maiorpot,sub,novapot,cont;

	numdec=0;
	
	while (op!= SAIR)
	{	
		scanf("%d", &op); //Lê a opção selecionada.
		
		switch(op) 
		{
			case OPC_BINARIO_DECIMAL:
				numdec=0;
				scanf ("%d",&qntddigbin);
				if (qntddigbin==0)
				{break;}
				else
				{
					basebin=qntddigbin-1;
					for (contbin=0;contbin<qntddigbin;contbin++,basebin--)//recebe cada digito em binario
					{
						scanf ("%d",&bin);
						if (bin==0)
						{
							base2=0;
						}
						else
						{
							base2 = pow(2,basebin);
						}
						numdec = numdec + base2;
						//printf ("contbin%d:\n",contbin);
					}
					printf ("%d\n",numdec);
					break;
				}
		
			case OPC_DECIMAL_BINARIO:
					scanf ("%d",&qntddigbin);
					scanf ("%d",&decimal);
					maiorpot=0;
					for (potdec=0;maiorpot<=decimal;potdec++)
					{
						maiorpot = pow(2,potdec);
					}
					//printf ("1");
					novapot = potdec - 1;//maior potencia menor que o numero em decimal
					//printf ("novapot: %d",novapot);
					//printf ("qntddigbin: %d",qntddigbin);
					if (qntddigbin>novapot)
					{
						for (;qntddigbin>novapot;qntddigbin--)
						{
							printf ("0");
						}
						
						sub = decimal-(maiorpot-pow(2,novapot));
						novapot = novapot-1;
						//maiorpot = pow(2,novapot);
						for (;maiorpot>1;novapot--)
						{
							maiorpot = pow(2,novapot);
							//printf ("maiorpot:%d\n",maiorpot);
							//o resto da subtração do num decimal tem que ser maior q 2 elevado a n para por 1 no bin,caso contario sera 0
							/*if (maiorpot==1)
							{
								printf ("0\n");
							}
							else
							{*/
								if (maiorpot>sub)
								{
									printf ("0");
								}
								else 
								{
									printf ("1");
									sub = sub - maiorpot; 
								}
						
							//printf ("sub:%d\n",sub);
						}
						printf ("\n");
						}//if
					else
					{
						if (qntddigbin<novapot+1)
						{
							sub = decimal-(maiorpot-pow(2,novapot));
							novapot = novapot-1;
							//maiorpot = pow(2,novapot);
							for (cont=0;cont<qntddigbin;novapot--,cont++)
							{
								maiorpot = pow(2,novapot);
								//printf ("maiorpot:%d\n",maiorpot);
								//o resto da subtração do num decimal tem que ser maior q 2 elevado a n para por 1 no bin,caso contario sera 0
								/*if (maiorpot==1)
								{
									printf ("0\n");
								}
								else
								{*/
									if (maiorpot>sub)
									{
										printf ("0");
									}
									else 
									{
										printf ("1");
										sub = sub - maiorpot; 
									}
						
								//printf ("sub:%d\n",sub);
							}
							printf ("\n");
						
						}
						else
						{
							if (qntddigbin==novapot+1)
							{
								sub = decimal-(maiorpot-pow(2,novapot));
								novapot = novapot-1;
								//maiorpot = pow(2,novapot);
								for (;maiorpot>1;novapot--)
								{
									maiorpot = pow(2,novapot);
									//printf ("maiorpot:%d\n",maiorpot);
									//o resto da subtração do num decimal tem que ser maior q 2 elevado a n para por 1 no bin,caso contario sera 0
									/*if (maiorpot==1)
									{
										printf ("0\n");
									}
									else
									{*/
										if (maiorpot>sub)
										{
											printf ("0");
										}
										else 
										{
											printf ("1");
											sub = sub - maiorpot; 
										}
						
									//printf ("sub:%d\n",sub);
								}
								printf ("\n");	
							}//if
						}//else
					}//else
				break;
		
			default:
				return (0);
		}//switch

	}//while
	

  return (0);
}
