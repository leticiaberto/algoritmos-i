/* ================================================================== *
    Universidade Federal de Sao Carlos - UFSCar, Sorocaba

    Disciplina: Algoritmos e Programação 1
    Prof. Tiago A. Almeida

    Lista 06 - Exercício 01 - Série de termos

    Instrucoes
    ----------

    Este arquivo contem o codigo que auxiliara no desenvolvimento do
    exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
    Dados do aluno:

    RA: 587354
    Nome: Leticia Mara Berto

* ================================================================== */

#include <stdio.h>
#include <math.h>

int main(){
	
	float x,n,resultpar,resultimpar,fat,contfat,soma;
	int resto,cont;

	scanf ("%f",&x);
	scanf ("%f",&n);

	fat = 1;
	resultimpar = 0;
	resultpar = 0;
	soma = 0;

	//o contador começa valendo 2 (fixo na formula) e vai aumentando. O cont sera o expoente
	for (cont=2;cont<=n+1;cont++)//vai ate n+1 pois o expoente ja começa em 2
	{
		//printf ("cont: %d\n",cont);
		//contfat=1;
		fat=1;
		resto=0;

		for (contfat=1;contfat<cont;contfat++)
		{
			fat = fat * contfat;
			//printf ("fat: %f\n",fat);
		}//for do fatorial

		resto = cont % 2;
	
		if (resto==0)//se os expoentes forem pares a formula tem que ser negativa
		{
			resultpar =  (-1)* ((pow(x,cont))/fat);
			//printf ("\n%fpar:",resultpar);
			soma = soma + resultpar;
		}
		else 
		{
			resultimpar = ((pow(x,cont))/fat);//se os expoentes forem impares a formula é positiva
			//printf ("\n%fimpar",resultimpar);
			soma = soma + resultimpar;
		}
		// result = resultpar + resultimpar;
		 //soma = result;
	
	}
	
	printf ("%.3f\n",soma);//resposta final

	return (0);
}
