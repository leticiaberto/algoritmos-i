/* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 1
	Prof. Tiago A. Almeida

	Lista 06 - Exercício 04 - Propriedade do 3025

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354
	Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>
#include <math.h>

//Definindo a frase para quando não existir nenhum número com a propriedade
#define FRASE_NAO_EXISTE "nao existe\n" 

/* ================================================================== */
/* ======================== ROTINA PRINCIPAL ======================== */
/* ================================================================== */

int main()
{
	 
	int d1,d2,n1,n2,troca,d1d2quad,cont,flags=0,flagn=1;

	scanf ("%d",&n1);
	scanf ("%d",&n2);
	
	if (n1>n2)//para deixar o intervalo correto - ordem crescente
	{
		troca=n1;
		n1=n2;
		n2=troca;
	}//if

	for (cont=n1;cont<=n2;cont++)
	{
		d1=cont%100;//pega os dois ultimos digitos
		d2=(cont-d1)/100;//pega os dois primeiros digitos

		d1d2quad= pow(d1+d2,2);

		if (d1d2quad==cont)
		{
			printf ("%d\n",d1d2quad);
			flags=1;
		}
		else
		{
			flagn=0;
		}
		
		//reseta as variaveis para o proximo calculo
		d1=0;
		d2=0;
		d1d2quad=0;
	}//for
	
	if (flagn==0 && flags==0)
	{
		printf (FRASE_NAO_EXISTE);
	}
	return 0;
}
