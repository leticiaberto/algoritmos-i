/* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 1
	Prof. Tiago A. Almeida

	Lista 06 - Exercício 02 - Coeficiente Binomial

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354
	Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>

//Definindo a frase de entrada invalida (n ou k negativos ou n<k)
#define FRASE_ERRO "Erro: entrada incorreta!\n" 

/* ================================================================== */
/* ======================== ROTINA PRINCIPAL ======================== */
/* ================================================================== */

int main(){

	int n,k,cb,fatn,fatk,fatnk,nk,i;

	scanf ("%d",&n);
	scanf ("%d",&k);

	fatn = 1;
	fatk = 1;
	fatnk = 1;
	cb=0;
	//i=0;
	
	//verifica se as variaveis sao validas
		while ((k<0) || (n<0) || (k>n))
		{
			printf (FRASE_ERRO);
			scanf ("%d",&n);
			scanf ("%d",&k);
		}
	while ((n!=0)||(k!=0))
	{
		//verifica se as variaveis sao validas
		while ((k<0) || (n<0) || (k>n))
		{
			printf (FRASE_ERRO);
			scanf ("%d",&n);
			scanf ("%d",&k);

			//caso os valores sejam zero o prog é interrompido
			

		}//while entradas invalidas

		if ((n==0)&&(k==0))
		{
			return(0);
		}//if

		nk = n - k;
/*************************************************************************/
		//calcula o fatorial de N
		for (i=2;i<=n;i++)
		{
			fatn *= i;
		}//for de N
		if((n == 0) || (n == 1))
			fatn = 1;
/*************************************************************************/
		
		//calcula o fatorial de K
		for (i=2;i<=k;i++)
		{
			fatk *= i;
		}//for de K
		if((k == 0) || (k == 1))
			fatk = 1;
/*************************************************************************/
		//calcula o fatorial de N-K
		for (i=2;i<=nk;i++)
		{
			fatnk *= i;
		}//for de N-K
		if((nk == 0) || (nk == 1))
			fatnk = 1;
/*************************************************************************/

		//calculo do cb a partir da formula fornecida no enunciado
		cb=fatn/(fatk*fatnk);

		printf ("%d\n",cb);//impressaõ do resultado final

		//leitura de novos valores
		scanf ("%d",&n);
		scanf ("%d",&k);

		//zera as variaveis para o proximo calculo
		nk=0;
		fatn = 1;
		fatk = 1;
		fatnk = 1;
		cb=0;
	}//while - comando de parada n=0 e k=0
		
	return (0);
}
