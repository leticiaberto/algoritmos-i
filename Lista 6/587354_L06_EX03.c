/* ================================================================== *
  Universidade Federal de Sao Carlos - UFSCar, Sorocaba

  Disciplina: Algoritmos e Programação 1
  Prof. Tiago A. Almeida

  Lista 06 - Exercício 03 - Menor natural divisivel por 1, ..., n

  Instrucoes
  ----------

  Este arquivo contem o codigo que auxiliara no desenvolvimento do
  exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
  Dados do aluno:

  RA: 587354
  Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>

#define TRUE  1
#define FALSE 0

/* ================================================================== */
/* ======================== ROTINA PRINCIPAL ======================== */
/* ================================================================== */

int main() 
{
	int n1,n2,troca,cont,i,flag=0,aux;

		
	//teste de consistencia de variaveis
	do//n1 tem que ser maior que zero
	{
		scanf ("%d",&n1);
		
	}while (n1<0);

	do//n2 tem que ser maior que zero
	{
		scanf ("%d",&n2);
		
	}while (n2<0);
	
	i=0;

	
	if (n1>n2)//se n1 for maior que n2 tem que inverter os valores para dar certo o intervalo
	{	
		troca=n1;
		n1=n2;
		n2=troca;
	}

	while (flag!=1)//quando for true significa que ele encontrou o menor numero
	{
		i++;
		for (cont=n1;cont<=n2;cont++)//intervalo entre n1 e n2
		{
			aux=i%cont;
			if (aux==0)
			{
				flag=1;
			}
			else 
			{
				flag=0;
				break;//se nao for divisivel ja sai do for para somar um ao numero
				//soma um no numero caso nao seja divisivel pelo intervalo
			}
		}
		
	}
	printf ("%d\n",i);//menor numero divisivel pelo intervalo
  	
	return (0);
}
