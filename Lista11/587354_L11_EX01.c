/* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 1
	Prof. Tiago A. Almeida

	Lista 11 - Exercício 01 - Cadeias de Caracteres

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354
	Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


//Definindo tamanho maximo da string
#define MAX 51



//Definindo opções
enum{
	OPC_SAIR,
	OPC_CONTAR_LETRAS,
	OPC_CONTAR_VOGAIS,
	OPC_CONTAR_CONSOANTES,
	OPC_CONTAR_PALAVRAS,
	OPC_OCORRENCIAS_STRING,
	OPC_CONTAR_MAIUSCULOS,
	OPC_CONTAR_MINUSCULOS,
	OPC_CONTAR_PALAVRAS_VOGAIS,
	OPC_CONTAR_PALAVRAS_CONSOANTES,
	OPC_TAMANHO,
	OPC_CONVERTER_MAIUSCULO,
	OPC_CONVERTER_MINUSCULO,
	OPC_PRIMEIRAS_LETRAS_EM_MAIUSCULAS,
	OPC_IMPRIMIR,
	OPC_IMPRIMIR_LEXICOGRAFICA,
	OPC_IMPRIMIR_INVERTIDA,
	OPC_IMPRIMIR_VOGAIS,
	OPC_IMPRIMIR_CONSOANTES,
	OPC_IMPRIMIR_PRIMEIRA_PALAVRA,
	OPC_IMPRIMIR_ULTIMA_PALAVRA,
	OPC_IMPRIMIR_INICIAIS,
	OPC_OCULTAR_VOGAIS,
	OPC_OCULTAR_CONSOANTES,
	OPC_CONCATENAR,
	OPC_COMPARAR_STRINGS,
	OPC_VERIFICAR_PALINDROMO,
	OPC_VERIFICAR_SIMETRIA,
	OPC_PRIMEIRA_OCORRENCIA,
	OPC_ULTIMA_OCORRENCIA,
	OPC_TROCAR_CARACTER,
	OPC_RETIRAR_CARACTER,
	OPC_ALTERNAR_MAIUS_MINUS
};


/* ---------------------- */
/* DECLARACAO DAS FUNCOES */
/* ---------------------- */

int contarCaractere (char string[], char caractere);
int contarVogais(char string[]);
int contarConsoantes(char string[]);
int contarPalavras(char string[]);
int contarOcorrenciasString(char string[], char stringAux[]);
int contarMaiusculos(char string[]);
int contarMinusculos(char string[]);
int contarPalavrasVogais(char string[]);
int contarPalavrasConsoantes(char string[]);
int tamanho(char string[]);
void converterMaiusculo(char string[]);
void converterMinusculo(char string[]);
void converterPrimeirasLetrasEmMaiusculas(char string[]);
void imprimir(char string[]);
void imprimirLexicografica(char string[]);
void imprimirInvetida(char string[]);
void imprimirVogais(char string[]);
void imprimirConsoantes(char string[]);
void imprimirPrimeiraPalavra(char string[]);
void imprimirUltimaPalavra(char string[]);
void imprimirIniciais(char string[]);
void ocultarVogais(char string[]);
void ocultarConsoantes(char string[]);
void concatenar(char string[], char stringAux[]);
int compararStrings(char string[], char stringAux[]);
int verificarPalindromo(char string[]);
int verificarSimetria(char string[]);
int primeiraOcorrencia(char string[], char caractere);
int ultimaOcorrencia(char string[], char caractere);
void trocarCaractere(char string[], char caractere, char caractere2);
void retirarCaractere(char string[], char caractere);
void alternarMaiusMinus(char string[]);
/* ------------------------------ */
/* ROTINA PRINCIPAL - NÃO ALTERAR */
/* ------------------------------ */
int main(){
	int opcao;
	char string[MAX], stringAux[MAX], caractere, caractere2;

	scanf("%[^\n]s", string);

	scanf("%d", &opcao);

	while(opcao != OPC_SAIR){

		switch(opcao){
			//------------------------------------------------------------------------------
			case OPC_CONTAR_LETRAS:
				scanf("\n%c", &caractere);
				printf("%d\n", contarCaractere(string, caractere));
			break;

			//------------------------------------------------------------------------------
			case OPC_CONTAR_VOGAIS:
				printf("%d\n", contarVogais(string));
			break;

			//------------------------------------------------------------------------------
			case OPC_CONTAR_CONSOANTES:
				printf("%d\n", contarConsoantes(string));
			break;

			//------------------------------------------------------------------------------
			case OPC_CONTAR_PALAVRAS:
				printf("%d\n", contarPalavras(string));
			break;

			//------------------------------------------------------------------------------
			case OPC_OCORRENCIAS_STRING:
				scanf("\n%[^\n]s", stringAux);
				printf("%d\n", contarOcorrenciasString(string, stringAux));
			break;

			//------------------------------------------------------------------------------
			case OPC_CONTAR_MAIUSCULOS:
				printf("%d\n", contarMaiusculos(string));
			break;

			//------------------------------------------------------------------------------
			case OPC_CONTAR_MINUSCULOS:
				printf("%d\n", contarMinusculos(string));
			break;

			//------------------------------------------------------------------------------
			case OPC_CONTAR_PALAVRAS_VOGAIS:
				printf("%d\n", contarPalavrasVogais(string));
			break;

			//------------------------------------------------------------------------------
			case OPC_CONTAR_PALAVRAS_CONSOANTES:
				printf("%d\n", contarPalavrasConsoantes(string));
			break;

			//------------------------------------------------------------------------------
			case OPC_TAMANHO:
				printf("%d\n", tamanho(string));
			break;

			//------------------------------------------------------------------------------
			case OPC_CONVERTER_MAIUSCULO:
				converterMaiusculo(string);
			break;

			//------------------------------------------------------------------------------
			case OPC_CONVERTER_MINUSCULO:
				converterMinusculo(string);
			break;

			//------------------------------------------------------------------------------
			case OPC_PRIMEIRAS_LETRAS_EM_MAIUSCULAS:
				converterPrimeirasLetrasEmMaiusculas(string);
			break;

			//------------------------------------------------------------------------------
			case OPC_IMPRIMIR:
				imprimir(string);
			break;

			//------------------------------------------------------------------------------
			case OPC_IMPRIMIR_LEXICOGRAFICA:
				imprimirLexicografica(string);
			break;

			//------------------------------------------------------------------------------
			case OPC_IMPRIMIR_INVERTIDA:
				imprimirInvetida(string);
			break;

			//------------------------------------------------------------------------------
			case OPC_IMPRIMIR_VOGAIS:
				imprimirVogais(string);
			break;

			//------------------------------------------------------------------------------
			case OPC_IMPRIMIR_CONSOANTES:
				imprimirConsoantes(string);
			break;

			//------------------------------------------------------------------------------
			case OPC_IMPRIMIR_PRIMEIRA_PALAVRA:
				imprimirPrimeiraPalavra(string);
			break;

			//------------------------------------------------------------------------------
			case OPC_IMPRIMIR_ULTIMA_PALAVRA:
				imprimirUltimaPalavra(string);
			break;

			//------------------------------------------------------------------------------
			case OPC_IMPRIMIR_INICIAIS:
				imprimirIniciais(string);
			break;

			//------------------------------------------------------------------------------
			case OPC_OCULTAR_VOGAIS:
				ocultarVogais(string);
			break;

			//------------------------------------------------------------------------------
			case OPC_OCULTAR_CONSOANTES:
				ocultarConsoantes(string);
			break;

			//------------------------------------------------------------------------------
			case OPC_CONCATENAR:
				scanf("\n%[^\n]s", stringAux);
				concatenar(string, stringAux);
			break;

			//------------------------------------------------------------------------------
			case OPC_COMPARAR_STRINGS:
				scanf("\n%[^\n]s", stringAux);
				printf("%d\n", compararStrings(string, stringAux));
			break;

			//------------------------------------------------------------------------------
			case OPC_VERIFICAR_PALINDROMO:
				printf("%d\n", verificarPalindromo(string));
			break;

			//------------------------------------------------------------------------------
			case OPC_VERIFICAR_SIMETRIA:
				printf("%d\n", verificarSimetria(string));
			break;

			//------------------------------------------------------------------------------
			case OPC_PRIMEIRA_OCORRENCIA:
				scanf("\n%c", &caractere);
				printf("%d\n", primeiraOcorrencia(string, caractere));
			break;

			//------------------------------------------------------------------------------
			case OPC_ULTIMA_OCORRENCIA:
				scanf("\n%c", &caractere);
				printf("%d\n", ultimaOcorrencia(string, caractere));
			break;

			//------------------------------------------------------------------------------
			case OPC_TROCAR_CARACTER:
				scanf("\n%c %c", &caractere, &caractere2);
				trocarCaractere(string, caractere, caractere2);
			break;

			//------------------------------------------------------------------------------
			case OPC_RETIRAR_CARACTER:
				scanf("\n%c", &caractere);
				retirarCaractere(string, caractere);
			break;

			//------------------------------------------------------------------------------
			case OPC_ALTERNAR_MAIUS_MINUS:
				alternarMaiusMinus(string);
			break;

		}

		scanf("%d", &opcao);
	}

	return 0;
}

/* ------------------------- */
/* IMPLEMENTACAO DAS FUNCOES */
/* ------------------------- */

int contarCaractere (char string[], char caractere)
{
	int cont=0,i;
	for (i=0;string[i] != '\0';i++)
	{
		if (string[i]==caractere)
			cont++;
	}
	return (cont);
}
int contarVogais(char string[])
{
	int i=0,cont=0;

	for (i=0;string[i] != '\0';i++)
	{
		if ((string[i] == 'A') || (string[i] == 'a')  || (string[i] == 'E')  || (string[i] == 'e')  || (string[i] == 'I')  || (string[i] == 'i')  || (string[i] == 'O')  || (string[i] == 'o')  || (string[i] == 'U')  || (string[i] == 'u') )
			cont++;
	}	
	return (cont);
}
int contarConsoantes(char string[])
{
	int i=0,cont=0;

	for (i=0;string[i] != '\0';i++)
	{
		if ((string[i] != 'A') && (string[i] != 'a')  && (string[i] != 'E')  && (string[i] != 'e')  && (string[i] != 'I')  && (string[i] != 'i')  && (string[i] != 'O')  && (string[i] != 'o')  && (string[i] != 'U')  && (string[i] != 'u') && (string[i] != ' ') && isalpha(string[i]))
			cont++;
	}	
	return (cont);
}
int contarPalavras(char string[])
{
	int i=0,cont=1;//admite que ja tem uma palavra
	for (i=0;string[i] != '\0';i++)//percorrer toda a string
	{	if (i==0 && string[i] == ' ')//se começar com espaço nao conta
			cont--;
		if ((string[i] == ' ') && (string[i+1] != ' ')&& (string[i+1] != '\0')) //se tiver espaço em branco ja conta uma palavra, e se tiver uma palavra e um espaço em branco sem uma palavra dps nao conta
			cont++;
	}
	
	return (cont);
}
int contarOcorrenciasString(char string[], char stringAux[])//terminar
{
	int i,j,cont = 0,cont2 = 0,tam;
	tam = strlen(stringAux);
	for(i = 0; string[i] != '\0'; i++)
	{
		cont = 0;//zera a cada repetição para poder usar no cont2 dps
		for(j = 0; j < tam; j++)
		{
			if((string[i + j]) == (stringAux[j]))//transforme em maiusculo pq é case sensitive
			{
				cont++;//achou uma posição com o mesmo valor
				if(cont == tam)//se for igual ao tamanho diz q encontrou a substring completa
					cont2++;//soma a quantidade de vezes que a substring apareceu
			}
			else
				break;//ja sai do laço pois nao encontrou o valor e nao precisa ficar fazendo
		}
	}
	return(cont2);
}
int contarMaiusculos(char string[])
{
	int i=0,cont=0;
	char c;
	for (i=0;string[i] != '\0';i++)
	{
		c = string[i];
		if (isupper(c))
			cont++;
	}
	return (cont);
}
int contarMinusculos(char string[])
{
	int i=0,cont=0;
	char c;
	for (i=0;string[i] != '\0';i++)
	{
		c = string[i];
		if (islower(c))
			cont++;
	}
	return (cont);
}
int contarPalavrasVogais(char string[])
{
	int i=0,cont=0,tam;//admite que ja tem uma palavra
	tam = strlen(string);
	//vai de tras para frente a partir do tamanho da string. Se for '' oi i=0,entra para contar uma palavra. Se for i==0 verifica o valor no i, se for '' verifica no i+1(volta uma posição para pegar a primeira letra palavra anterior)
	for (i=tam-1;i>=0;i--)
	{	
		if (((string[i] == ' ') && (string[i-1] != ' '))|| (i==0)) //se tiver espaço em branco ja conta uma palavra, e se tiver uma palavra e um espaço em branco sem uma palavra dps nao conta
		{			
			if (i==0)
			{//printf("entrou");
				if ((string[i] == 'A') || (string[i] == 'a')  || (string[i] == 'E')  || (string[i] == 'e')  || (string[i] == 'I')  || (string[i] == 'i')  || (string[i] == 'O')  || (string[i] == 'o')  || (string[i] == 'U')  || (string[i] == 'u') )
					cont++;	
			}
			else
			{
				if ((string[i+1] == 'A') || (string[i+1] == 'a')  || (string[i+1] == 'E')  || (string[i+1] == 'e')  || (string[i+1] == 'I')  || (string[i+1] == 'i')  || (string[i+1] == 'O')  || (string[i+1] == 'o')  || (string[i+1] == 'U')  || (string[i+1] == 'u'))
					cont++;
			}
		}
	}
	return(cont);
}
int contarPalavrasConsoantes(char string[])
{
	int i=0,cont=0,tam;//admite que ja tem uma palavra
	tam = strlen(string);
	//vai de tras para frente a partir do tamanho da string. Se for '' oi i=0,entra para contar uma palavra. Se for i==0 verifica o valor no i, se for '' verifica no i+1(volta uma posição para pegar a primeira letra palavra anterior)
	for (i=tam-1;i>=0;i--)
	{	
		if (((string[i] == ' ') && (string[i-1] != ' '))|| (i==0)) //se tiver espaço em branco ja conta uma palavra, e se tiver uma palavra e um espaço em branco sem uma palavra dps nao conta
		{			
			if (i==0)
			{//printf("entrou");
				if ((string[i] != 'A') && (string[i] != 'a')  && (string[i] != 'E')  && (string[i] != 'e')  && (string[i] != 'I')  && (string[i] != 'i')  && (string[i] != 'O')  && (string[i] != 'o')  && (string[i] != 'U')  && (string[i] != 'u') )
					cont++;	
			}
			else
			{
				if ((string[i+1] != 'A') && (string[i+1] != 'a')  && (string[i+1] != 'E')  && (string[i+1] != 'e')  && (string[i+1] != 'I')  && (string[i+1] != 'i')  && (string[i+1] != 'O')  && (string[i+1] != 'o')  && (string[i+1] != 'U')  && (string[i+1] != 'u') && (string[i+1] != ' ') && (string[i+1] != '\0'))
					cont++;
			}
		}
	}
	return(cont);
}
int tamanho(char string[])
{
	int tam;
	tam = strlen(string);
	return (tam);
}
void converterMaiusculo(char string[])
{
	int i=0;
	char c;
	while (string[i]!= '\0')
	{
		c = string[i];
		string[i] = toupper(c);
		i++;
	}
	//puts (string);
}
void converterMinusculo(char string[])
{
	int i=0;
	char c;
	while (string[i]!= '\0')
	{
		c = string[i];
		string[i] = tolower(c);
		i++;
	}
	//puts (string);
}
void converterPrimeirasLetrasEmMaiusculas(char string[])
{
	int i=0,tam;//admite que ja tem uma palavra
	char c;
	tam = strlen(string);
	for (i=tam-1;i>=0;i--)
	{	
		if (((string[i] == ' ') && (string[i-1] != ' '))|| (i==0)) //se tiver espaço em branco ja conta uma palavra, e se tiver uma palavra e um espaço em branco sem uma palavra dps nao conta
		{			
			if (i==0)
			{//printf("entrou");
				c = string[i];
				string[i] = toupper(c);
			}
			else
			{
				c = string[i+1];
				string[i+1] = toupper(c);
			}
		}
	}
	//puts(string);
}
void imprimir(char string[])
{
	puts (string);
}
void imprimirLexicografica(char string[])
{/*
	int i=0,cont=1,j=0,k=0,m;//admite que ja tem uma palavra
	char palavras[MAX][MAX],aux,result[MAX];//ira armazenar as palavras
	for (i=0;string[i] != '\0';i++)//percorrer toda a string
	{
		palavras[j][k]=string[i];k++;
		if ((string[i] == ' ') && (string[i+1] != ' ')&& (string[i+1] != '\0')) //se tiver espaço em branco ja conta uma palavra, e se tiver uma palavra e um espaço em branco sem uma palavra dps nao conta
			j++;
	}
	for (i=0;i<j;i++)
	{
		if ((strcmp(palavras[j][k], palavras[j+1][k])) > 0)
		{
			aux = palavras[j];
        		palavras[j]= palavras[j + 1];
        		palavras[j + 1]= aux;
		}
	}
	for (i=0;i<j;i++)
	{
		
		printf("%s",palavras[i]);
		
	}
	    //puts (palavras);
*/

}
void imprimirInvetida(char string[])
{
	char strinvertida[MAX];
	int i,tam,j=0;
	tam = strlen(string);
	for (i=tam-1;i>=0;i--)
	{	//printf ("string: %c\n",string[i]);
		strinvertida[j] = string[i];
		j++;
	}
	puts (strinvertida);
}
void imprimirVogais(char string[])
{
	int i,j=0;
	char aux[MAX];
	for (i=0;aux[i] != '\0';i++)//"zera" a string inicial para receber a auxiliar com os novos valores
	{
		aux[i] = '\0';
	}
	for (i=0;string[i] != '\0';i++)
	{
			if ((string[i] == 'A') || (string[i] == 'a')  || (string[i] == 'E')  || (string[i] == 'e')  || (string[i] == 'I')  || (string[i] == 'i')  || (string[i] == 'O')  || (string[i] == 'o')  || (string[i] == 'U')  || (string[i] == 'u') )
			{
				aux[j] = string[i];
				j++;
			}
	}
	puts (aux);
}
void imprimirConsoantes(char string[])
{
	int i,j=0;
	char aux[MAX];
	for (i=0;aux[i] != '\0';i++)//"zera" a string inicial para receber a auxiliar com os novos valores
	{
		aux[i] = '\0';
	}
	for (i=0;string[i] != '\0';i++)
	{
		if ((string[i] != 'A') && (string[i] != 'a')  && (string[i] != 'E')  && (string[i] != 'e')  && (string[i] != 'I')  && (string[i] != 'i')  && (string[i] != 'O')  && (string[i] != 'o')  && (string[i] != 'U')  && (string[i] != 'u'))
		{
			aux[j] = string[i];
			j++;
		}
	}
	puts (aux);
}
void imprimirPrimeiraPalavra(char string[])
{
	int i=0,tam,flag=0;//admite que ja tem uma palavra
	char c;
	tam = strlen(string);
	for (i=tam-1;i>=0;i--)
	{	
		if (((string[i] == ' ') && (string[i-1] != ' '))|| (i==0)) //se tiver espaço em branco ja conta uma palavra, e se tiver uma palavra e um espaço em branco sem uma palavra dps nao conta
		{			
			if (i==0 && flag==0)
			{//printf("entrou");
				c = string[i];
				string[i] = toupper(c);
			}
			else
			{
				c = string[i+1];
				string[i+1] = toupper(c);
			}
		}
	}
	puts(string);
}
void imprimirUltimaPalavra(char string[])
{
	int i=0,j=0,k=0,tam,flag=0;//admite que ja tem uma palavra
	char aux[MAX],result[MAX];
	tam = strlen(string);
	for (i=tam-1;i>=0 && flag==0;i--)
	{	aux[j] = string[i];//pega os valores de tras pra frente
		j++;
		
		if (((string[i] == ' ') && (string[i-1] != ' '))|| (i==0)) //se tiver espaço em branco ja conta uma palavra, e se tiver uma palavra e um espaço em branco sem uma palavra dps nao conta
		{			
			if (i==0)
			{
				for (j=j-1;j>=0;j--)
				{
					result[k] = aux[j];//joga em outra string na ordem certa 
					k++;
				}
				puts(result);
				flag=1;//indica q ja pegou a ultima palavra
				
			}
			else
			{//printf("j: %d",j);
				for (j=j-1;j>=0;j--)
				{
					result[k] = aux[j-1];//joga em outra string na ordem certa e ignora o espaço em branco entreuma palavra e outra (j-1 no indice)
					k++;
				}
				puts(result);
				flag=1;//indica q ja pegou a ultima palavra
			}
			
		}
	}
}
void imprimirIniciais(char string[])
{
	int i=0,j=0,k=0,tam;//admite que ja tem uma palavra
	char aux[MAX],result[MAX];
	tam = strlen(string);
	for (i=tam-1;i>=0;i--)
	{	
		if (((string[i] == ' ') && (string[i-1] != ' '))|| (i==0)) //se tiver espaço em branco ja conta uma palavra, e se tiver uma palavra e um espaço em branco sem uma palavra dps nao conta
		{			
			if (i==0)
			{//printf("entrou");
				aux[j] = string[i];
				j++;
			}
			else
			{
				aux[j] = string[i+1];
				j++;
			}
		}
	}
	for (j=j-1;j>=0;j--)
	{
		result[k] = aux[j];//joga em outra string na ordem certa 
		k++;
	}
	puts(result);
}
void ocultarVogais(char string[])
{
	int i=0;
	char result[MAX];
	for (i=0;string[i] != '\0';i++)
	{
		if ((string[i] == 'A') || (string[i] == 'a')  || (string[i] == 'E')  || (string[i] == 'e')  || (string[i] == 'I')  || (string[i] == 'i')  || (string[i] == 'O')  || (string[i] == 'o')  || (string[i] == 'U')  || (string[i] == 'u') )
			result[i] = '*';
		else
			result[i] = string[i];
	}
	result[i] = '\0';
	puts (result);	
}
void ocultarConsoantes(char string[])
{
	int i=0;
	char result[MAX];
	for (i=0;string[i] != '\0';i++)
	{
		if ((string[i] != 'A') && (string[i] != 'a')  && (string[i] != 'E')  && (string[i] != 'e')  && (string[i] != 'I')  && (string[i] != 'i')  && (string[i] != 'O')  && (string[i] != 'o')  && (string[i] != 'U')  && (string[i] != 'u') && (string[i] != ' ') && (string[i] != '\0') )
			result[i] = '*';
		else
			result[i] = string[i];		
	}
	result[i] = '\0';
	puts (result);	
}
void concatenar(char string[], char stringAux[])
{
	strcat (string,stringAux);
	//puts (string);
}
int compararStrings(char string[], char stringAux[])//verificar o q deve retornar
{
	int result;
	if (strcmp (string, stringAux)==0)
		result = 1;
	else
		result = 0;
	return(result);//se retornar 1 é pq são iguais
	
}
int verificarPalindromo(char string[])
{
	int i,j=0,k=0,cont=1,result;//assume que nao é palindromo
	char auxpalin[MAX],auxstring[MAX];
	//tam = strlen(string);
	for (i=0;string[i] != '\0';i++)
	{
		if (string[i] != ' ' && string[i] != '\0')//elimina os espaços em branco da string original
		{
			auxstring[j] = string[i];
			j++;
			cont++;
		}
		auxstring[j]='\0';//para finalizar a string ja q a ultima posição esta vazia
	}//puts(auxstring);
	for (i=j-1;i>=0;i--)//inverte a string original. J-1 para nao pegar o \0
	{	//c = string [i];
		auxpalin[k] = auxstring[i];
		k++;
	}
	//puts (auxpalin);
	if( strcmp (auxstring, auxpalin)==0)
		result = 1;
	else
		result = 0;
	return (result);//se retornar 1 é pq é palindromo
}
int verificarSimetria(char string[])
{
	 int i,j,tam,div=0,cont = 0;

   	 tam = strlen(string);

   	 if((tam % 2) != 0)//se for impar ja não é simetrico
       		 return(0);//sai da função
   	 else
   	 {
       		 div = (int) tam / 2;//para separar a palavra na metade
	        for(i = 0, j = tam - 1; j >= div; i++, j--)//um contador pega a ultima letra e o outro pega a primeira e vai comparando os indices de acordo com a condição
           	{
			 if(string[i] == string[j])
               			 cont++;
		}
   	 }
   	 if(cont == div)
        	return(1);//é simétrico
    	else
       		return(0);
}
int primeiraOcorrencia(char string[], char caractere)
{
	int i,po=-1,flag=0;
	for (i=0;string[i] != '\0' && flag==0;i++)
	{
		if (string[i] == caractere)
		{
			po = i;
			flag = 1;
		}
	}
	return (po);
}
int ultimaOcorrencia(char string[], char caractere)
{
	int i,uo=-1;
	for (i=0;string[i] != '\0';i++)
	{
		if (string[i] == caractere)
			uo = i;
	}
	return (uo);
}
void trocarCaractere(char string[], char caractere, char caractere2)
{
	int i;
	for (i=0;string[i] != '\0';i++)
	{
		if (string[i] == caractere)
			string[i] = caractere2;
	}
	//puts (string);
}
void retirarCaractere(char string[], char caractere)
{
	int i,j=0,k=0;
	char aux[MAX];
	for (i=0;string[i] != '\0';i++)//armazena em uma string auxiliar sem a letra q o usuario digitar
	{
		if (string[i] != caractere)
		{
			aux[j] = string[i];
			j++;
		}
	}
	aux[j] = '\0';//fecha a ultima posição que ficou sem valor
	for (i=0;string[i] != '\0';i++)//"zera" a string inicial para receber a auxiliar com os novos valores
	{
		string[i] = '\0';
	}

	for (j=0;aux[j] != '\0';j++)//recebe os novos valores
	{
		
			 string[k] = aux[j];
			k++;
	}string[k] = '\0';//fecha a ultima posição que ficou sem valor
	//puts (string);
}
void alternarMaiusMinus(char string[])
{
	int i;
	char c;
	for (i=0;string[i] != '\0';i++)
	{
		c = string[i];
		if (islower(c))
		{
			string[i] = toupper(c);
		}
		if (isupper(c))
		{
			string[i] = tolower(c);
		}
	}
	//puts (string);
}

	
