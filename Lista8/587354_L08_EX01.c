/* ================================================================== *
    Universidade Federal de Sao Carlos - UFSCar, Sorocaba

    Disciplina: Algoritmos e Programação 1
    Prof. Tiago A. Almeida

    Lista 08 - Exercício 01 - Manipulacao de vetores

    Instrucoes
    ----------

    Este arquivo contem o codigo que auxiliara no desenvolvimento do
    exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
    Dados do aluno:

    RA: 587354
    Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>
#include <math.h>

#define MAX 100   // Limite de tamanho dos vetores A e B
#define OPC_INVALIDA "Opcao invalida\n" // Frase de opcao invalida


//Enumeração das opções do programa
enum{
	OPC_SAIR,				//Finaliza o programa.
	OPC_SOMA_A,				//Imprime a soma dos valores do vetor A.
	OPC_SOMA_B,				//Imprime a soma dos valores do vetor B.
	OPC_SOMA, 				//Imprime a soma dos valores dos vetores.
	OPC_MEDIA_A,			//Imprime a média dos valores do vetor A.
	OPC_MEDIA_B,			//Imprime a média dos valores do vetor B.
	OPC_MEDIA,				//Imprime a média dos valores presentes nos vetores.
	OPC_DESVIO_A,			//Imprime o desvio padrão dos valores do vetor A.
	OPC_DESVIO_B,			//Imprime o desvio padrão dos valores do vetor B.
	OPC_DESVIO,				//Imprime o desvio padrão dos valores dos vetores.
	OPC_MAX_A,				//Imprime o maior valor do vetor A.
	OPC_MAX_B,				//Imprime o maior valor do vetor B.
	OPC_MAX,				//Imprime o maior valor presente nos vetores.
	OPC_MIN_A,				//Imprime o menor valor do vetor A.
	OPC_MIN_B,				//Imprime o menor valor do vetor B.
	OPC_MIN,				//Imprime o menor valor presente nos vetores.
	OPC_IMPRIMIR_A,			//Imprime os elementos do vetor A.
	OPC_IMPRIMIR_B,			//Imprime os elementos do vetor B.
	OPC_IMPRIMIR_INVERT_A,	//Imprime os elementos do vetor A em ordem invertida.
	OPC_IMPRIMIR_INVERT_B,	//Imprime os elementos vetor B em ordem invertida.
	OPC_IMPRIMIR_PARES_A,	//Imprime somente os números pares do vetor A.
	OPC_IMPRIMIR_PARES_B,	//Imprime somente os números pares do vetor B.
	OPC_IMPRIMIR_IMPARES_A,	//Imprime somente os números ímpares do vetor A.
	OPC_IMPRIMIR_IMPARES_B,	//Imprime somente os números ímpares do vetor B.
	OPC_IMPRIMIR_PRIMOS_A,	//Imprime somente os números primos do vetor A.
	OPC_IMPRIMIR_PRIMOS_B,	//Imprime somente os números primos do vetor B.
};



int main(){
	int tamA, tamB, //Tamanho dos vetores A e B.
		vetA[MAX], vetB[MAX], //Vetores A e B.
		i, j, //Contadores para os laços.
		opc, //Armazena a opção selecionada.
		somaA, somaB, soma, //Armazena as somas dos valores dos vetores A, B e geral.
		numValores, //Armazena o número de valores diferentes nos vetores de cada vetor.
		maiorA, maiorB, maior, //Armazena os maiores valores do vetores A, B e geral.
		menorA, menorB, menor, //Armazena os menores valores do vetores A, B e geral.
		divisor,aux; //Auxiliar no cálculo dos primos.
	
	float mediaA, mediaB, media, //Armzenam as médias dos valores dos vetores A, B e geral.
		  desvioA, desvioB, desvio; //Armazena os valores do desvio padrão dos valores dos vetores A, B e geral.
	

	somaA = 0;
	somaB = 0;
	// Recebe o tamanho de A e os valores de A
	do
	{
		scanf ("%d",&tamA);
	}while (tamA<1 || tamA>100);

	for (i=0;i<tamA;i++)
	{
		scanf ("%d",&vetA[i]);
	}

	// Recebe o tamanho de B e os valores de B
	do
	{
		scanf ("%d",&tamB);
	}while (tamB<1 || tamB>100);

	for (i=0;i<tamB;i++)
	{
		scanf ("%d",&vetB[i]);
	}


	scanf("%d", &opc); //Lê a opção selecionada pelo usuário.

	//Executa o programa enquanto a opção selecionada for diferente de zero.
	while(opc != OPC_SAIR){
		somaA = 0;
		somaB = 0;
		soma = 0;
		mediaA = 0;
		mediaB = 0;
		media = 0;
		desvioA = 0;
		desvioB = 0;
 		desvio = 0;
		switch(opc){
			//Soma dos valores do vetor A--------------------
			case OPC_SOMA_A:
				for (i=0;i<tamA;i++)
				{
					somaA = somaA + vetA[i];
				}
				printf("%d\n", somaA);
			break;
			//-----------------------------------------------

			//Soma dos valores do vetor B--------------------
			case OPC_SOMA_B:
				for (i=0;i<tamB;i++)
				{
					somaB = somaB + vetB[i];
				}
				printf("%d\n", somaB);
			break;
			//-----------------------------------------------

			//Soma dos valores dos vetores--------------------
			case OPC_SOMA:
				for (i=0;i<tamA;i++)
				{
					somaA = somaA + vetA[i];
				}
								
				for (i=0;i<tamB;i++)
				{
					somaB = somaB + vetB[i];
				}
				soma = somaA + somaB;

				printf("%d\n", soma);
			break;
			//-----------------------------------------------

			//Média dos valores de A-------------------------
			case OPC_MEDIA_A:
				for (i=0;i<tamA;i++)
				{
					somaA = somaA + vetA[i];
				}
				mediaA = somaA/(float)tamA;
				printf("%.2f\n", mediaA);
			break;
			//-----------------------------------------------

			//Média dos valores de B-------------------------
			case OPC_MEDIA_B:
				for (i=0;i<tamB;i++)
				{
					somaB = somaB + vetB[i];
				}
				mediaB = somaB/(float)tamB;
				printf("%.2f\n", mediaB);
			break;
			//-----------------------------------------------

			//Média dos valores dos vetores------------------
			case OPC_MEDIA:
				for (i=0;i<tamA;i++)
				{
					somaA = somaA + vetA[i];
				}
				for (i=0;i<tamB;i++)
				{
					somaB = somaB + vetB[i];
				}
				soma = somaA + somaB;
				media = soma / (float)(tamA + tamB);
				printf("%.2f\n", media);
			break;
			//-----------------------------------------------

			//Desvio padrão dos valores de A-----------------
			case OPC_DESVIO_A:
				for (i=0;i<tamA;i++)
				{
					somaA = somaA + vetA[i];
				}
				//printf ("soma: %d\n",somaA);
				mediaA = somaA/(float)tamA;
				//printf ("media: %f\n",mediaA);
				//calcula a diferença do valor do vetor e a média,eleva o resultado ao quadrado e soma os valores para encontrar o desvio padrao
				for (i=0;i<tamA;i++)
				{
					desvioA = desvioA +  pow((vetA[i] - mediaA),2);
					
					//printf ("vet - media: %f\n",vetA[i] - mediaA);
				}
				desvioA = sqrt(desvioA/(tamA-1));
				printf("%.2f\n", desvioA);

			break;
			//-----------------------------------------------

			//Desvio padrão dos valores de B-----------------
			case OPC_DESVIO_B:
				for (i=0;i<tamB;i++)
				{
					somaB = somaB + vetB[i];
				}
				mediaB = somaB/(float)tamB;
				//calcula a diferença do valor do vetor e a média,eleva o resultado ao quadrado e soma os valores para encontrar o desvio padrao
				for (i=0;i<tamB;i++)
				{
					desvioB = desvioB +  pow((vetB[i] - mediaB),2);
				}
				desvioB = sqrt(desvioB/(tamB-1));
				printf("%.2f\n", desvioB);

			break;
			//-----------------------------------------------

			//Desvio padrão dos valores dos vetores-------
			case OPC_DESVIO:
				for (i=0;i<tamA;i++)
				{
					somaA = somaA + vetA[i];
				}
				for (i=0;i<tamB;i++)
				{
					somaB = somaB + vetB[i];
				}
				soma = somaA + somaB;
				media = soma / (float)(tamA + tamB);

				for (i=0;i<tamA;i++)
				{
					desvio = desvio +  pow((vetA[i] - media),2);
				}

				for (i=0;i<tamB;i++)
				{
					desvio = desvio +  pow((vetB[i] - media),2);
				}
				desvio = sqrt(desvio/(tamA + tamB - 1));
				printf("%.2f\n", desvio);
			break;
			//-----------------------------------------------

			//Maior valor do vetor A-------------------------
			case OPC_MAX_A:
				maiorA = vetA[0];//atribui o primeiro valor do vetor como menor valor
				for (i=1;i<tamA;i++)
				{
					if (vetA[i] > maiorA)
						maiorA = vetA[i];
				}
				printf("%d\n", maiorA);
			break;
			//-----------------------------------------------

			//Maior valor do vetor B-------------------------
			case OPC_MAX_B:
				maiorB = vetB[0];//atribui o primeiro valor do vetor como menor valor
				for (i=1;i<tamB;i++)
				{
					if (vetB[i] > maiorB)
						maiorB = vetB[i];
				}
				printf("%d\n", maiorB);
			break;
			//-----------------------------------------------

			//Maior valor dos vetores------------------------
			case OPC_MAX:
				maior = vetA[0];//atribui o primeiro valor do vetor como menor valor
				for (i=1;i<tamA;i++)
				{
					if (vetA[i] > maior)
						maior = vetA[i];
				}
				for (i=1;i<tamB;i++)
				{
					if (vetB[i] > maior)
						maior = vetB[i];
				}
				printf("%d\n", maior);
			break;
			//-----------------------------------------------

			//Menor valor do vetor A-------------------------
			case OPC_MIN_A:
				menorA = vetA[0];//atribui o primeiro valor do vetor como menor valor
				for (i=1;i<tamA;i++)
				{
					if (vetA[i] < menorA)
						menorA = vetA[i];
				}
				printf("%d\n", menorA);
			break;
			//-----------------------------------------------

			//Menor valor do vetor B-------------------------
			case OPC_MIN_B:
				menorB = vetB[0];//atribui o primeiro valor do vetor como menor valor
				for (i=1;i<tamB;i++)
				{
					if (vetB[i] < menorB)
						menorB = vetB[i];
				}
				printf("%d\n", menorB);
			break;
			//-----------------------------------------------

			//Menor valor dos vetores------------------------
			case OPC_MIN:
				menor = vetA[0];//atribui o primeiro valor do vetor como menor valor
				for (i=1;i<tamA;i++)
				{
					if (vetA[i] < menor)
						menor = vetA[i];
				}
				for (i=1;i<tamB;i++)
				{
					if (vetB[i] < menor)
						menor = vetB[i];
				}
				printf("%d\n", menor);
			break;
			//-----------------------------------------------

			//Imprime os valores de A------------------------
			case OPC_IMPRIMIR_A:
				for (i=0;i<tamA;i++)
				{
					printf ("%d ",vetA[i]);
				}
				printf("\n");
			break;
			//-----------------------------------------------

			//Imprime os valores de B------------------------
			case OPC_IMPRIMIR_B:
				for (i=0;i<tamB;i++)
				{
					printf ("%d ",vetB[i]);
				}
				printf("\n");
			break;
			//-----------------------------------------------

			//Imprime os valores de A invertidos-------------
			case OPC_IMPRIMIR_INVERT_A:
				for (i=tamA-1;i>=0;i--)
				{
					printf ("%d ",vetA[i]);
				}
				printf("\n");
			break;
			//-----------------------------------------------

			//Imprime os valores de B invertidos-------------
			case OPC_IMPRIMIR_INVERT_B:
				for (i=tamB-1;i>=0;i--)
				{
					printf ("%d ",vetB[i]);
				}
				printf("\n");
			break;
			//-----------------------------------------------

			//Imprime os valores pares de A------------------
			case OPC_IMPRIMIR_PARES_A:
				for (i=0;i<tamA;i++)
				{
					if (vetA[i] % 2 == 0)
					printf ("%d ",vetA[i]);
				}
				printf("\n");
			break;
			//-----------------------------------------------

			//Imprime os valores pares de B------------------
			case OPC_IMPRIMIR_PARES_B:
				for (i=0;i<tamB;i++)
				{
					if (vetB[i] % 2 == 0)
					printf ("%d ",vetB[i]);
				}
				printf("\n");
			break;
			//-----------------------------------------------

			//Imprime os valores ímpares de A----------------
			case OPC_IMPRIMIR_IMPARES_A:
				for (i=0;i<tamA;i++)
				{
					if (vetA[i] % 2 != 0)
					printf ("%d ",vetA[i]);
				}
				printf("\n");
			break;
			//-----------------------------------------------

			//Imprime os valores ímpares de B----------------
			case OPC_IMPRIMIR_IMPARES_B:
				for (i=0;i<tamB;i++)
				{
					if (vetB[i] % 2 != 0)
					printf ("%d ",vetB[i]);
				}
				printf("\n");
			break;
			//-----------------------------------------------

			//Imprime os elementos primos de A---------------
			case OPC_IMPRIMIR_PRIMOS_A:
				//verifica se há valores iguais no vetor				
				for (i=0;i<tamA;i++)
				{
					aux = vetA[i];
					for (j=i+1;j<tamA;j++)
					{
						if (vetA[j]==aux)
						vetA[j]=1;
					}
				}

				for (i=0;i<tamA;i++)//percorre o vetor A
				{	
					if (vetA[i]==1 || vetA[i]==0)
					divisor = 0;
					else
					divisor = 1;
					for (j=2;j<vetA[i] && divisor==1;j++)
					{
						if (vetA[i] % j ==0 || vetA[i]==0)
						divisor = 0;
					}
					if (divisor==1)
					printf ("%d ",vetA[i]);
				}
				printf("\n");
			break;
			//-----------------------------------------------

			//Imprime os elementos primos de B---------------
			case OPC_IMPRIMIR_PRIMOS_B:
				//verifica se há valores iguais no vetor e troca por 1 pra nao fazer o mesmo numero primo				
				for (i=0;i<tamB;i++)
				{
					aux = vetB[i];
					for (j=i+1;j<tamB;j++)
					{
						if (vetB[j]==aux)
						vetB[j]=1;
					}
				}

				for (i=0;i<tamB;i++)//percorre o vetor B
				{	
					if (vetB[i]==1 || vetB[i]==0)
					divisor = 0;
					else
					divisor = 1;
					for (j=2;j<vetB[i] && divisor==1;j++)
					{
						if (vetB[i] % j ==0 || vetB[i]==0) 
						divisor = 0;
					}
					if (divisor==1)
					printf ("%d ",vetB[i]);
				}
				printf("\n");
			break;
			//-----------------------------------------------

			//Opção inválida---------------------------------
			default:
				if (opc != 0)
					printf(OPC_INVALIDA);
			//-----------------------------------------------

		}

		scanf("%d", &opc); //Lê novamente a opção do usuário.
	}

	return 0;
}
