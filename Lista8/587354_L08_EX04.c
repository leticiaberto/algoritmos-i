/* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 1
	Prof. Tiago A. Almeida

	Lista 08 - Exercício 04 - Polinômios

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354
	Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>
#include <math.h>


#define TAM 10  //Quantidade maximo de termos por polinomio
#define FRASE_OPC_INVALIDA "Opcao invalida\n" //Mensagem de erro em caso de opcao invalida


// Opcoes do menu    
enum{
	OPC_SAIR,					//Finaliza o programa.
	OPC_SOMA,					//A(x) + B(x)
	OPC_SUBTRACAO,				//A(x) - B(x)
	OPC_RESOLVE_A,				//Resolver A(x) para um dado x
	OPC_RESOLVE_B,				//Resolver B(x) para um dado x
	OPC_RESOLVE_C,				//Resolver C(x) para um dado x
};


//Registro que representa um termo do polinomio
typedef struct Termo{
	int coef;
	int expo;
} Polinomio;


int main(){
	
	Polinomio polinomioA[TAM], polinomioB[TAM], polinomioC[2*TAM]; //Polinomios A, B e C
	
	int i, j, k=0, //Contadores e iteradores
		num_termosA=0, num_termosB=0, num_termosC=0, //Número de termos dos polinômios
		resultado, //Resultados dos polinômios resolvidos
		x, //valor de x para resolver os polinômios
		opcao; //opção do menu


	//le o numero de termos de A(x) e em seguida cada termo (coeficiente, expoente)
	do
	{
		scanf("%d",&num_termosA);
	}while(num_termosA<1 || num_termosA>10);
	
	for (i=0;i<num_termosA;i++)
	{
		scanf ("%d %d ",&polinomioA[i].coef,&polinomioA[i].expo);
	
	}

	//le o numero de termos de B(x) e em seguida cada termo (coeficiente, expoente)
	do
	{
		scanf("%d",&num_termosB);
	}while(num_termosB<1 || num_termosB>10);
	
	for (i=0;i<num_termosB;i++)
	{
		scanf ("%d %d",&polinomioB[i].coef,&polinomioB[i].expo);
	}


	do {
	
		//Solicita opcao para o usuario
		scanf("%d", &opcao);


		switch(opcao){
			// Opcao Sair
			case OPC_SAIR: return 0;

			//A(x) + B(x) 
			case OPC_SOMA:
				//define o tamanho do vetor C
				if (num_termosA>num_termosB)
				{
					num_termosC = num_termosA;

					for (j=0;j<num_termosA;j++)
					{ 
						polinomioC[j].coef = 0;
						polinomioC[j].expo = polinomioA[j].expo;
						for (i=0;i<num_termosB;i++)
						{
							
							if (polinomioA[j].expo == polinomioB[i].expo)
								{
									polinomioC[j].coef = polinomioA[j].coef + polinomioB[i].coef;
									break;
								}
							else
								polinomioC[j].coef = polinomioA[j].coef;
						}
					}
				}
				else
				{
					if (num_termosB>num_termosA)
					{	num_termosC = num_termosB;
						
						for (j=0;j<num_termosB;j++)
						{ 
							polinomioC[j].coef = 0;
							polinomioC[j].expo = polinomioB[j].expo;
							for (i=0;i<num_termosA;i++)
							{
							
								if (polinomioB[j].expo == polinomioA[i].expo)
								{
									polinomioC[j].coef = polinomioB[j].coef + polinomioA[i].coef;
										break;
								}
								else
									polinomioC[j].coef = polinomioB[j].coef;
							}
						}
					}
					else
					{
						/*if (num_termosA==num_termosB)
						{*/	num_termosC = num_termosA;
							for (j=0;j<num_termosA;j++)
							{ 
								polinomioC[j].expo = polinomioB[j].expo;
								polinomioC[j].coef = polinomioA[j].coef + polinomioB[j].coef;
							}
					}
				}
				
				for (i=0;i<num_termosC;i++)
				{
					if (polinomioC[i].expo==0)
						printf ("+%d ",polinomioC[i].coef);
					else
						if (polinomioC[i].expo==1)
							printf ("+%dx ",polinomioC[i].coef);
					
						else
							if (polinomioC[i].coef>0 || polinomioC[i].coef==0)
								printf ("+%dx^%d ",polinomioC[i].coef,polinomioC[i].expo);
							else
								printf ("%dx^%d ",polinomioC[i].coef,polinomioC[i].expo);
				}	
				printf ("\n");
				break;

			//A(x) - B(x) 
			case OPC_SUBTRACAO: 
				//define o tamanho do vetor C
				if (num_termosA>num_termosB)
				{
					num_termosC = num_termosA;

					for (j=0;j<num_termosA;j++)
					{ 
						polinomioC[j].coef = 0;
						polinomioC[j].expo = polinomioA[j].expo;
						for (i=0;i<num_termosB;i++)
						{
							
							if (polinomioA[j].expo == polinomioB[i].expo)
								{
									polinomioC[j].coef = polinomioA[j].coef - polinomioB[i].coef;
									break;
								}
							else
								polinomioC[j].coef = polinomioA[j].coef;
						}
					}
				}
				else
				{
					if (num_termosB>num_termosA)
					{	num_termosC = num_termosB;
						
						for (j=0;j<num_termosB;j++)
						{ 
							polinomioC[j].coef = 0;
							polinomioC[j].expo = polinomioB[j].expo;
							for (i=0;i<num_termosA;i++)
							{
							
								if (polinomioB[j].expo == polinomioA[i].expo)
									{
										polinomioC[j].coef = polinomioA[i].coef - polinomioB[j].coef;
										break;
									}
								else
									polinomioC[j].coef = -(polinomioB[j].coef);
							}
						}
					}
					else
					{
						/*if (num_termosA==num_termosB)
						{*/	num_termosC = num_termosA;
							for (j=0;j<num_termosA;j++)
							{ 
								polinomioC[j].expo = polinomioB[j].expo;
								polinomioC[j].coef = polinomioA[j].coef - polinomioB[j].coef;
							}
					}
				}
				for (i=0;i<num_termosC;i++)
				{
					if (polinomioC[i].expo==0)
						printf ("+%d ",polinomioC[i].coef);
					else
						if (polinomioC[i].expo==1 && polinomioC[i].coef==1)
							printf ("+%dx ",polinomioC[i].coef);
						else
							if (polinomioC[i].expo==1 && polinomioC[i].coef!=1 && polinomioC[i].coef!=-1)
							printf ("+%dx ",polinomioC[i].coef);
						else
							if (polinomioC[i].expo==1 && polinomioC[i].coef==-1)
								printf ("%dx ",polinomioC[i].coef);
						else
							if (polinomioC[i].coef>0 || polinomioC[i].coef==0)
								printf ("+%dx^%d ",polinomioC[i].coef,polinomioC[i].expo);
						else
							printf ("%dx^%d ",polinomioC[i].coef,polinomioC[i].expo);
				}		
				printf ("\n");
				
				break;				

			//A(x) para dado x
			case OPC_RESOLVE_A: 
					resultado = 0;
					scanf ("%d",&x);
					for (i=0;i<num_termosA;i++)
					{
						resultado = resultado + (polinomioA[i].coef * (pow(x,polinomioA[i].expo)));
					}
					printf ("%d\n",resultado);
				break;
			

			//B(x) para dado x
			case OPC_RESOLVE_B: 
					resultado = 0;
					scanf ("%d",&x);
					for (i=0;i<num_termosB;i++)
					{
						resultado = resultado + (polinomioB[i].coef * (pow(x,polinomioB[i].expo)));
					}
					printf ("%d\n",resultado);
				break;
			

			//C(x) para dado x
			case OPC_RESOLVE_C: 
					resultado = 0;
					scanf ("%d",&x);
					for (i=0;i<num_termosC;i++)
					{
						resultado = resultado + (polinomioC[i].coef * (pow(x,polinomioC[i].expo)));
					}
					printf ("%d\n",resultado);

				
				break;

			default:
				printf(FRASE_OPC_INVALIDA);
				break;

		}

	} while(opcao!=0);
}
