/* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 1
	Prof. Tiago A. Almeida

	Lista 08 - Exercício 02 - Operacoes com vetores

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354
	Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>


#define MAX 100  //Tamanho maximo dos vetores
#define FRASE_ENTRADA_INVALIDA "Erro: Entrada invalida!\n" //Frase de opcao invalida


//Enumeração das opções do programa
enum{
	OPC_SAIR,				//Finaliza o programa.
	OPC_SOMA,				//A + B
	OPC_SUBTRACAO,			//A - B
	OPC_PRODUTO,			//A .* B
	OPC_DIVISAO,			//A ./ B
};


int main(){
	int opcao,				//Opção do menu desejada
		tam_A=0, tam_B=0,	//Tamanho desejado dos vetores A e B
		i;					//Contador (iterador) para os lacos

	float vet1[MAX], 			//Primeiro vetor
		  vet2[MAX], 			//Segundo vetor
		  vet_result[MAX];		//Vetor com os resultados das operacoes


	//Inicializacao dos vetores A e B com zero em todas as posicoes
	for (i=0;i<MAX;i++)
	{
		vet1[i]=0;
	}

	for (i=0;i<MAX;i++)
	{
		vet2[i]=0;
	}
	//Le o tamanho e os valores de A
	do
	{
		scanf ("%d",&tam_A);
	}while(tam_A<=0 || tam_A>100);
	
	for (i=0;i<tam_A;i++)
	{
		scanf ("%f",&vet1[i]);
	}


	//Le o tamanho e os valores de B
	/* <<< COMPLETE AQUI >>> */
	do
	{
		scanf ("%d",&tam_B);
	}while(tam_B<=0 || tam_B>100);
	
	for (i=0;i<tam_B;i++)
	{
		scanf ("%f",&vet2[i]);
	}


	//Le a opcao desejada
	scanf("%d", &opcao);
	do{
		switch(opcao){
			case OPC_SAIR: return (0); //Finaliza o programa

			case OPC_SOMA: //A + B
					for (i=0;i<MAX;i++)
					{	
						vet_result[i] = vet1[i] + vet2[i];	
					}
				break;

			case OPC_SUBTRACAO: //A - B
				for (i=0;i<MAX;i++)
					{	
						vet_result[i] = vet1[i] - vet2[i];	
					}
				break;

			case OPC_PRODUTO: //A .* B
				for (i=0;i<MAX;i++)
					{	
						vet_result[i] = vet1[i] * vet2[i];	
					}
				break;

			case OPC_DIVISAO: //A ./ B
					for (i=0;i<MAX;i++)
					{	
						if (vet2[i]==0)
						vet_result[i] = 0;
						else
						vet_result[i] = vet1[i] / vet2[i];	
					}
				break;

			default:
				printf(FRASE_ENTRADA_INVALIDA); //Imprime frase de opcao invalida
		}


		//Imprime o resultado das operações
		if (opcao == 1 || opcao == 2 || opcao == 3 || opcao == 4){
			if(tam_A > tam_B)
			{
				for(i=0; i<tam_A; i++)
					printf("%.2f ", vet_result[i]);

				printf("\n");
			} 
			else 
			{
				for(i=0; i<tam_B; i++)
					printf("%.2f ", vet_result[i]);

				printf("\n");
			}
		}
		
		
		//Recebe uma nova opcao
		scanf("%d", &opcao);

	} while(opcao != 0);
}
