/* ================================================================== *
    Universidade Federal de Sao Carlos - UFSCar, Sorocaba

    Disciplina: Algoritmos e Programação 1
    Prof. Tiago A. Almeida

    Lista 04 - Exercício 05 - Calculadora simples

    Instrucoes
    ----------

    Este arquivo contem o codigo que auxiliara no desenvolvimento do
    exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
    Dados do aluno:

    RA: 587354
    Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>

// Frase exibida para operacao invalida (constante)
#define MSG_ERRO "operacao nao reconhecida\n"


/* ================================================================== */
/* ======================== ROTINA PRINCIPAL ======================== */
/* ================================================================== */

int main()
{    
    
    float num1,num2,result;
    char operacao;
    result = 0;
    scanf ("%f %c %f",&num1,&operacao,&num2);

    if ((operacao != '+') && (operacao != '-') && (operacao != '/') && (operacao != '*'))
    {
	printf (MSG_ERRO);
    }
    else
	if (operacao == '+')
	{
		result = num1+num2;
		printf ("%.2f",result);
	}	
    else
	if (operacao == '-')
	{
		result = num1-num2;
		printf ("%.2f",result);
	}	
   else
	if (operacao == '/')
	{
		result = num1/num2;
		printf ("%.2f",result);
	}	
   else
	if (operacao == '*')
	{
		result = num1*num2;
		printf ("%.2f",result);
	}	
    return (0);
}    
