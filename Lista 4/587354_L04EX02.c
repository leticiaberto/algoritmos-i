 /* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 1
	Prof. Tiago A. Almeida

	Lista 04 - Exercício 02 - Classes Sociais

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354
	Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>

/* ================================================================== */
/* ======================== ROTINA PRINCIPAL ======================== */
/* ================================================================== */

int main () 
{
    float renda;
    char ca,cb,cc,cd,ce;
    
    ca='A';
    cb='B';
    cc='C';
    cd='D';
    ce='E';
    scanf ("%f",&renda);
    
    if (renda<1085.01)
    {
    	printf ("%c",ce);
    }
    else 
    	if ((renda>=1085.01)&&(renda<=1734.00))
    	{
    		printf ("%c",cd);
   	 }
    	else 
    		if ((renda>=1734.01)&&(renda<=7475.00))
    		{
    			printf ("%c",cc);
   		 }
    		else 
    			if ((renda>7475.00)&&(renda<=9745.00))
    			{
    				printf ("%c",cb);
    			}
    			else 
    				if (renda>9745.00)
    				{
    					printf ("%c",ca);
    				}
    
    return (0);
}



