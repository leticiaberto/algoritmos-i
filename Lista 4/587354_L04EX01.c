/* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 1
	Prof. Tiago A. Almeida

	Lista 04 - Exercício 01 - Aumento de Salário

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354
	Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>

/* ================================================================== */
/* ======================== ROTINA PRINCIPAL ======================== */
/* ================================================================== */

int main () 
{

    float sal_atual,novo_sal;
	
    scanf ("%f",&sal_atual);

    if (sal_atual <2000)
    {
	novo_sal=sal_atual*1.2;
    }
    else
    {
	novo_sal=sal_atual*1.15;
    }
    printf ("%.2f",novo_sal);
    return (0);
}

