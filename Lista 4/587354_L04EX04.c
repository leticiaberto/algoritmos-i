/* ================================================================== *
    Universidade Federal de Sao Carlos - UFSCar, Sorocaba

    Disciplina: Algoritmos e Programação 1
    Prof. Tiago A. Almeida

    Lista 04 - Exercício 04 - Data mais antiga

    Instrucoes
    ----------

    Este arquivo contem o codigo que auxiliara no desenvolvimento do
    exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
    Dados do aluno:

    RA: 587354
    Nome: Letícia Mara Berto

* ================================================================== */

#include <stdio.h>

/* ================================================================== */
/* ======================== ROTINA PRINCIPAL ======================== */
/* ================================================================== */

int main () 
{

    int dia1,mes1,ano1,dia2,mes2,ano2;

    scanf ("%d/%d/%d",&dia1,&mes1,&ano1);
    scanf ("%d/%d/%d",&dia2,&mes2,&ano2);

    if (ano1<ano2)
    {
	printf("%.02d/%.02d/%.04d",dia1,mes1,ano1);
    }
    else
        if (ano2<ano1)
        {
		printf("%.02d/%.02d/%.04d",dia2,mes2,ano2);
        }
   else
	if ((ano1=ano2)&&(mes1<mes2))
        {
		printf("%.02d/%.02d/%.04d",dia1,mes1,ano1);
	}
   else
	if ((ano1=ano2)&&(mes2<mes1))
        {
		printf("%.02d/%.02d/%.04d",dia2,mes2,ano2);
	}
   else
	if ((ano1=ano2)&&(mes1=mes2)&&(dia1<dia2))
        {
		printf("%.02d/%.02d/%.04d",dia1,mes1,ano1);
	}
   else
	printf("%.02d/%.02d/%.04d",dia2,mes2,ano2);
    return (0);
}
