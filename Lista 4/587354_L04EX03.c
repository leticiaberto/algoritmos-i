/* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 1
	Prof. Tiago A. Almeida

	Lista 04 - Exercício 03 - Indice de Massa Corporal

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354
	Nome: Leticia Mara Berto

* ================================================================== */

#include <stdio.h>

// Frase exibida para os possiveis resultados (constantes)
#define FRASE_ABAIXO	"abaixo\n"
#define FRASE_NORMAL	"normal\n"
#define FRASE_ACIMA		"acima\n"

// Frase exibida se o sexo informado for incorreto (diferente de 'F', 'f', 'M' ou 'm')
#define FRASE_SEXO_INCORRETO	"sexo incorreto\n"



/* ================================================================== */
/* ======================== ROTINA PRINCIPAL ======================== */
/* ================================================================== */
int main() {
	
	char sexo;
	float altura,peso,imc;

	scanf ("%c",&sexo);
	scanf ("%f",&peso);
	scanf ("%f",&altura);

	imc = peso/(altura*altura);

	if ((sexo == 'f') || (sexo =='F'))
	{
		if (imc < 20)
		{
			printf ("%.2f\n",imc);
			printf (FRASE_ABAIXO);
		}
		else 
			if ((imc >= 20) && (imc <= 24))
			{
				printf ("%.2f\n",imc);
				printf (FRASE_NORMAL);
			}
			else
			{
				printf ("%.2f\n",imc);
				printf (FRASE_ACIMA);
			}
	}//if se sexo feminino
	else
		if ((sexo == 'm') || (sexo =='M'))
		{
			if (imc < 20)
			{
				printf ("%.2f\n",imc);
				printf (FRASE_ABAIXO);
			}
			else 
				if ((imc >= 20) && (imc <= 23))
				{
					printf ("%.2f\n",imc);
					printf (FRASE_NORMAL);
				}
				else
				{
					printf ("%.2f\n",imc);
					printf (FRASE_ACIMA);
				}
		}//if se sexo masculino
		else
		{
			printf ("%.2f\n",imc);		
			printf (FRASE_SEXO_INCORRETO);
		}
	
	return (0);

}

