/* 14 - Escreva um programa que calcule o volume de uma esfera de raio R (real), onde R é
fornecido pelo usuário (utilize 2 casas decimais).
587354 - 10/04/2014 */

#include <stdio.h>
#include <math.h>
#define PI 3.14
int main()
{

	float raio,volume;

	scanf ("%f",&raio);
	volume =1.33*PI*(pow(raio,3)); //volume = 4/3*pi*raio³  4/3 = 1.33

	printf ("%.2f",volume);
	
	return (0);
}
