/* 12 Faça um programa que receba duas notas de um aluno (real) e seus respectivos pesos
(inteiro), calcule e imprima a média ponderada dessas notas (utilize 2 casas decimais).
587354 - 10/04/2014 */

#include <stdio.h>
int main ()
{
	float n1,n2,media;
	int p1,p2;

	scanf ("%f",&n1);
	scanf ("%d",&p1);
	scanf ("%f",&n2);
	scanf ("%d",&p2);

	media = (((n1*p1)+(n2*p2))/(p1+p2));

	printf ("%.2f",media);
	return (0);
}
