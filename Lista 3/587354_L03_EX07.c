/* Exercicio 7 da lista 3 - soma de inteiros
587354 - 10/04/2014 */

#include <stdio.h>
int main ()
{
	int num1, num2, soma;

	scanf ("%d", &num1);
	scanf ("%d", &num2);

	soma = num1 + num2;
	
	printf ("%d",soma);
	return (0);
}
