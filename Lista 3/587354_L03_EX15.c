/* 15 - Escreva um programa que receba o valor do salário de um funcionário (real) e o valor
do salário mínimo (real). Calcule e imprima quantos salários mínimos ganha esse funci-
onário (utilize 2 casas decimais). 
587354 - 10/04/2014 */

#include <stdio.h>
int main ()
{
	float sal_func,sal_min,qntd_salmin;

	scanf ("%f",&sal_func);
	scanf ("%f",&sal_min);
	
	qntd_salmin = sal_func/sal_min;

	printf("%.2f",qntd_salmin);
	return (0);
}
