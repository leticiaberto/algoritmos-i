/* 16 - Faça um programa que receba uma determinada hora (real - hora e minuto separados
por ponto), calcule e imprima essa hora em minutos (inteiro).
587354 - 10/04/2014 */

#include <stdio.h>
int main()
{
	int hora,minuto;
	int total_minutos;

	scanf ("%d.%d",&hora,&minuto);
	total_minutos =  hora*60 + minuto;

	printf ("%d",total_minutos);
	return (0);
}
