/* 17 Sabe-se que o quilowatt de energia custa um quinto do salário mínimo. Faça um pro-
grama que receba o valor do salário mínimo (real) e a quantidade de quilowatts gasta
por uma residência (real). Calcule e imprima (imprima um valor por linha e utilize 2 ca-
sas decimais):
a) o valor, em reais, de cada quilowatt;
b) o valor, em reais, a ser pago por essa residência;
c) o novo valor a ser pago por essa residência, a partir de um desconto de 15%.
587354 - 10/04/2014 */

#include <stdio.h>
int main ()
{
	float sal_min,qntd_kw,valor_kw,residencia,desconto;

	scanf ("%f",&sal_min);
	scanf ("%f",&qntd_kw);

	valor_kw = 0.2*sal_min;//1/5=0,2
	residencia =0.2*sal_min*qntd_kw;
	desconto =  residencia*0.85;

	printf ("%.2f",valor_kw);
	printf("\n%.2f",residencia);
	printf("\n%.2f",desconto);
	return (0);
}
