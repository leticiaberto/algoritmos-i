/* 13 - Escreva um programa que receba o valor de um depósito (real) e o valor da taxa de
juros (real, entre 0% e 100%). Calcule e imprima o valor do rendimento e o valor total
depois do rendimento (imprima um valor por linha e utilize 8 casas: 5 para a parte inteira
usando espaço em branco para alinhar à esquerda e 2 casas decimais).
587354 - 10/04/2014 */

#include <stdio.h>
int main ()
{
	float deposito,taxa_juros,rend,total;

	scanf ("%f",&deposito);
	scanf ("%f",&taxa_juros);
		
	rend = (taxa_juros * deposito)/100;
	total = deposito + rend;
	
	printf ("%8.02f",rend);
	printf ("\n%8.02f",total);//verificar casas decimais
	return (0);
}
