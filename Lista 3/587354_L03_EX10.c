 /* 10- Faça um programa que receba dois números inteiros, calcule e imprima (Imprima um
valor por linha e se a saída for real, utilize 2 casas decimais):

soma dos dois números;
subtração do primeiro pelo segundo;
subtração do segundo pelo primeiro;
multiplicação dos dois números;
divisão do primeiro pelo segundo;
divisão do segundo pelo primeiro;
quociente inteiro da divisão do primeiro pelo segundo;
quociente inteiro da divisão do segundo pelo primeiro;
resto da divisão do primeiro pelo segundo;
resto da divisão do segundo pelo primeiro.

587354 10/04/2014 */

#include <stdio.h>
int main ()
{
	int num1, num2,soma,sub_12,sub_21,mult,quoc_12,quoc_21,resto_12,resto_21;
	float div_12, div_21;

	scanf ("%d",&num1);
	scanf ("%d",&num2);

	soma = num1 + num2;
	sub_12 = num1 - num2;
	sub_21 = num2 - num1;
	mult = num1 * num2;
	div_12 = num1/(float)num2;
	div_21 = num2/(float)num1;
	quoc_12 = num1/num2;
	quoc_21= num2/num1;
	resto_12 = num1 % num2;
	resto_21= num2  % num1;

	printf ("\n%d",soma);
	printf ("\n%d",sub_12);
	printf ("\n%d",sub_21);
	printf ("\n%d",mult);
	printf ("\n%.2f",div_12);
	printf ("\n%.2f",div_21);
	printf ("\n%d",quoc_12);
	printf ("\n%d",quoc_21);
	printf ("\n%d",resto_12);
	printf ("\n%d",resto_21);

	return (0);
}
