/* 18 Faça um programa que receba o peso de uma pessoa em kg (real), calcule e imprima
(imprima um valor por linha e utilize 2 casas decimais):
a) o peso dessa pessoa em gramas;
b) seu novo peso em gramas se essa pessoa engordar 5%.
587354 - 10/04/2014 */

#include <stdio.h>
int main()
{
	float peso,gramas,novo_peso;

	scanf ("%f",&peso);
	
	gramas = peso * 1000;
	novo_peso= gramas * 1.05;

	printf ("%.2f",gramas);
	printf ("\n%.2f",novo_peso);
	return (0);
}
