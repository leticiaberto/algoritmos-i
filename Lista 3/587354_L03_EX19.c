/* 19 - Faça um programa para ler uma temperatura em graus Celsius (real) e imprimir em Fah-
renheit (real, utilize 2 casas decimais).
587354 - 10/04/2014 */

#include <stdio.h>
int main()
{
	float celsius,fahrenheit;

	scanf ("%f",&celsius);

	fahrenheit = (celsius * 1.8000) + 32;
	printf ("%.2f",fahrenheit);
	return (0);
}
