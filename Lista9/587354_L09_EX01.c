/* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 1
	Prof. Tiago A. Almeida

	Lista 09 - Exercício 01 - Operacoes com matrizes

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354
	Nome: Letícia Mara Berto

* ================================================================== */


#include <stdio.h>

//Definindo limite para ordem da matriz
#define MAX 20

//Definindo mensagem de erro
#define FRASE_ERRO "Opcao invalida!\n"

//Enumeração das opções do programa
enum{
	OPC_SAIR,				//Finaliza o programa.
	OPC_SOMA_LINHA,			//Soma de uma linha i.
	OPC_SOMA_COLUNA,		//Soma de uma coluna j.
	OPC_SOMA_GERAL,			//Soma de todos os valores da matriz.
	OPC_SOMA_DIAG_PRINC,	//Soma da diagonal principal.
	OPC_SOMA_DIAG_SEC,		//Soma da diagonal secundaria.
	OPC_MAXMINMED_LIN,		//Imprime os valores máximo, mínimo e médio da linha i
	OPC_MAXMINMED_COL,		//Imprime os valores máximo, mínimo e médio da coluna j
	OPC_MAXMINMED_GERAL,	//Imprime os valores máximo, mínimo e médio da matriz
	OPC_SOMA_MATRIZ,		//B = A + A.
	OPC_PROD_MATRIZ,		//B = A * A'.
	OPC_TRANSPOSTA,			//B = A'.
	OPC_PROD_K,				//B = k * A.
};


int main(){
	int matriz_A[MAX][MAX], //Matriz base (A)
		matriz_B[MAX][MAX], //Matriz resultante das operações (B)
		matriz_aux[MAX][MAX], //Matriz auxiliar na multiplicação (recebera a transposta de A)
		i, j, cont, //Contadores de linha, coluna e auxiliar da multiplicação
		ordem, //Ordem da matriz: qtde de linhas e colunas
		opcao, //Opcao desejada
		linha, coluna, k, //A linha, coluna ou valor desejados quando opção é 1, 2, 6, 7 ou 12
		soma, max, min; //Guarda a soma, o valor máximo e o valor mínimo
	float media;

	//Recebe o tamanho da matriz (m e n)
	do
	{
		scanf("%d", &ordem);
	}while(ordem>20);
	//Alimenta cada posicao da matriz com um inteiro
	for(i=1; i<=ordem; i++)
		for(j=1; j<=ordem; j++)
			scanf("%d", &matriz_A[i][j]);


	//Le a opcao desejada
	scanf("%d", &opcao);


	//Processa a opcao desejada
	while (opcao != OPC_SAIR){
		switch(opcao){
			
			case OPC_SOMA_LINHA:
				soma=0;
				scanf ("%d",&linha);
				for(i=1; i<=ordem; i++)
				{	for(j=1; j<=ordem; j++)
					{	if (i==linha)
							soma = soma + matriz_A[i][j];
					}
				}
				break;

			case OPC_SOMA_COLUNA:
				soma=0;
				scanf ("%d",&coluna);
				for(j=1; j<=ordem; j++)
				{
					for(i=1; i<=ordem; i++)
					{	if (j==coluna)
							soma = soma + matriz_A[i][j];
					}
				}		
				break;

			case OPC_SOMA_GERAL:
				soma=0;
				for(i=1; i<=ordem; i++)
				{
					for(j=1; j<=ordem; j++)
					{	soma = soma + matriz_A[i][j];
					}
				}	
				break;
			
			case OPC_SOMA_DIAG_PRINC:
				soma=0;
				for (i=1;i<=ordem;i++)
				{	for (j=1;j<=ordem;j++)
					{
						if (j==i)
							soma = soma + matriz_A[i][j];
					}
					
				}
				break;
			
			case OPC_SOMA_DIAG_SEC:
				  soma=0;
				  k=0;
				  for (i=1;i<=ordem;i++)
					{
						for (j=1;j<=ordem;j++)
						{
							if (j==ordem-k)
								soma = soma + matriz_A[i][j];
						}
						k++;
					}
				break;
			
			case OPC_MAXMINMED_LIN:
				media=0;
				scanf ("%d",&linha);
				max = -999999;
				min = 999999;
				for (i=1;i<=ordem;i++)
				{
					for (j=1;j<=ordem;j++)
					{
						if (i==linha)
						{	
							if (matriz_A[i][j] > max)
								max = matriz_A[i][j];
							if (matriz_A[i][j] < min)
								min = matriz_A[i][j];
							media = media + matriz_A[i][j];
						}
					}
				}
				media = media/ordem;
				break;

			case OPC_MAXMINMED_COL:
				media=0;
				scanf ("%d",&coluna);
				max = -999999;
				min = 999999;
				for (j=1;j<=ordem;j++)
				{
					for (i=1;i<=ordem;i++)
					{
						if (j==coluna)
						{	
							if (matriz_A[i][j] > max)
								max = matriz_A[i][j];
							if (matriz_A[i][j] < min)
								min = matriz_A[i][j];
							media = media + matriz_A[i][j];
						}
					}
				}
				media = media/ordem;
				break;

			case OPC_MAXMINMED_GERAL:
				media=0;
				max = -999999;
				min = 999999;
				for (j=1;j<=ordem;j++)
				{
					for (i=1;i<=ordem;i++)
					{							
							if (matriz_A[i][j] > max)
								max = matriz_A[i][j];
							if (matriz_A[i][j] < min)
								min = matriz_A[i][j];
							media = media + matriz_A[i][j];
						
					}
				}
				media = media/(ordem*ordem);
				break;

			case OPC_SOMA_MATRIZ:
				soma=0;
				for(i=1; i<=ordem; i++)
				{	for(j=1; j<=ordem; j++)
					{	
						matriz_B[i][j] = matriz_A[i][j] + matriz_A[i][j];
					}
				}
				break;

			case OPC_PROD_MATRIZ:
				for (i = 1; i <=ordem; i++)
				{
  					for (j = 1; j <=ordem; j++) 
					{
      						matriz_B[i][j] = 0;
  					}
				}
				for (i = 1; i <=ordem; i++)
				{
  					for (j = 1; j <=ordem; j++) 
					{
      						matriz_aux[i][j] = matriz_A[j][i];
  					}
				}

				for (i = 1; i <= ordem; i++) 
				{
  					for (j = 1; j <= ordem; j++) 
					{ 
  						for (k = 1; k <= ordem; k++) 
						{
    							matriz_B[i][j] = matriz_B[i][j] + (matriz_A[i][k] * matriz_aux[k][j]);
      						}
    					}
  				}
				break;

			case OPC_TRANSPOSTA:
				for (i = 1; i <=ordem; i++)
				{
  					for (j = 1; j <=ordem; j++) 
					{
      						matriz_B[i][j] = matriz_A[j][i];
  					}
				}
				break;

			case OPC_PROD_K:
				scanf("%d",&k);
				for (i = 1; i <=ordem; i++)
				{
  					for (j = 1; j <=ordem; j++) 
					{
      						matriz_B[i][j] = matriz_A[i][j]*k;
  					}
				}
				break;

			default: 
				printf(FRASE_ERRO);
		}	


		//Imprime na tela as respostas finais
		if(opcao>=OPC_SOMA_LINHA && opcao<=OPC_SOMA_DIAG_SEC)
			printf("%d\n", soma);
		else if(opcao>=OPC_MAXMINMED_LIN && opcao<=OPC_MAXMINMED_GERAL){
			printf("%d %d %.2f\n", min, max, media);
		} else if(opcao>=OPC_SOMA_MATRIZ && opcao<=OPC_PROD_K){
			for(i=1; i<=ordem; i++){
				for(j=1; j<=ordem; j++)
					printf("%3d ", matriz_B[i][j]);

				printf("\n");
			}
		}

		//Le a opcao desejada
		scanf("%d", &opcao);
	}
	return 0;
}
