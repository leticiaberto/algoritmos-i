/* ================================================================== *
	Universidade Federal de Sao Carlos - UFSCar, Sorocaba

	Disciplina: Algoritmos e Programação 1
	Prof. Tiago A. Almeida

	Lista 09 - Exercício 02 - Ponto de sela

	Instrucoes
	----------

	Este arquivo contem o codigo que auxiliara no desenvolvimento do
	exercicio. Voce precisara completar as partes requisitadas.

* ================================================================== *
	Dados do aluno:

	RA: 587354
	Nome: Letícia Mara Berto

* ================================================================== */


//Comando para incluir o pacote basico stdio.h onde estao
//os comandos necessarios para a criacao do programa.
#include <stdio.h>

//Ordem maxima da matriz. 
#define ORDEM_MAXIMA  50

//Resposta para a operacao que verifica 
//se a matriz apresenta pontos de sela.
#define FALSO           0
#define VERDADEIRO      1

//Resposta a serem impressas na tela
#define RESP_SIM	"sim\n"
#define RESP_NAO	"nao\n"

typedef struct _ponto
{
	int coluna;
	int linha;
}Ponto;

//Inicio
int main() {
		//Ponto resp;
		int matriz[ORDEM_MAXIMA][ORDEM_MAXIMA],pontos[ORDEM_MAXIMA],resp[ORDEM_MAXIMA],
		i,j,k,a,ordem,mincol,maxlin,minlin,maxcol,flag=FALSO,cont=0,maior,colj;

		//recebe a ordem da matriz
		do
		{
			scanf("%d",&ordem);
		}while(ordem>50);

		//recebe os valores da matriz
		for (i=1;i<=ordem;i++)
		{
			for(j=1;j<=ordem;j++)
			{
				scanf ("%d",&matriz[i][j]);
			}
		}
			
		//verfifica se tem ponto de sela
		for (i=1;i<=ordem;i++)
		{	
			maxlin = -999999;
			mincol = 9999999;
			maxcol = -999999;
			minlin = 9999999;	
			//encontra o maximo na linha e minimo na coluna
			for(j=1;j<=ordem;j++)
			{
				if(matriz[i][j]>maxlin)//encontra o maior valor da linha
				{
					maxlin = matriz[i][j];
					colj=j;
				}
			}//printf ("colj: %d\n",colj);
			 for (a=colj;a==colj;a++)//verifica a coluna onde foi encontrado o maior valor da linha
      			 {     
               			 for (k=1;k<=ordem;k++)
               			 {//printf ("mincol: %d\n",mincol);
				//printf ("matriz: %d\n",matriz[k][a]);
					if(matriz[k][a]<mincol)//encontra o menor valor da coluna
					{
						mincol = matriz[k][a];
					}
				 }
			}//printf ("maxlin: %d\n",maxlin);printf ("mincol: %d\n",mincol);
			if (mincol==maxlin)//se o valor encontrado for maximo na linha e minimo na coluna
			{
				//printf ("%d %d\n",i,j-1);
				flag=VERDADEIRO;
				pontos[i]= colj;//o indice é a linha e j-1 é a coluna
				cont++;
			}
			a=0;
			//encontra o minimo na linha e maximo na coluna
			for(j=1;j<=ordem;j++)
			{
				if(matriz[i][j]<minlin)//encontra o menor valor da linha
				{
					minlin = matriz[i][j];
					cont = j;
				}
			}//printf ("minlin: %d\n",minlin);
				a=cont;
               			 for (k=1;k<=ordem;k++)
               			 {
					if(matriz[k][a]>maxcol)//encontra o menor valor da coluna
					{
						maxcol = matriz[k][a];
					}
				 }
			//printf ("maxcol: %d\n",maxcol);
			if (maxcol==minlin)//se o valor encontrado for maximo na linha e minimo na coluna
			{
				//printf ("%d %d\n",i,a);
				flag=VERDADEIRO;
				pontos[i]= a;//o indice é a linha e "a" é a coluna
				cont++;
			}		
		}
		if (flag==1)//encontrou ponto de sela
		{
			printf (RESP_SIM);
		}
		else
			printf (RESP_NAO);	
		maior=999999;
		for (i=1;i<=ordem;i++)
		{
			if (pontos[i]!=0)
			{
				//printf ("%d\n",pontos[i]);
				if (pontos[i]<maior)
				{	resp[i] = pontos[i];
					maior = pontos[i]; 
				}
			}
		//printf ("%d \n",resp[i]);
		}
		for (i=ordem;i>0;i--)
		{
			if (pontos[i]!=0)
			{
				printf ("%d %d\n",i,resp[i]);
			}
		}
			
	return(0);
}
//Fim
